/* ============================================================================

 UDP communication for client

 Author: Gastone Pietro Rosati Papini

 ============================================================================ */

#ifndef __CODRIVER_CLIENT_FUNCTIONS_H
#define __CODRIVER_CLIENT_FUNCTIONS_H

#if defined(_WIN32)
#pragma comment (lib, "Ws2_32.lib")
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Winsock2.h>
#include <Ws2tcpip.h>

#elif defined(__MACH__) || defined(__linux__)
#include <arpa/inet.h>
#include <sys/socket.h>
#endif

#include "codriver_interfaces_data_structs.h"
#include "UDP_limit_functions.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DLL_EXPORT
#define USE_DLL __declspec(dllexport)
#elif DLL_IMPORT
#define USE_DLL __declspec(dllimport)
#else
#define USE_DLL
#endif

// Client init
USE_DLL int client_init(const char *server_ip, int server_port);

// Client send
USE_DLL int client_send(volatile UDP_UINT server_run, UDP_UINT message_id, input_data_str* scenario_msg);

// Client receive
USE_DLL int client_receive(UDP_UINT *server_run, UDP_UINT *message_id, output_data_str* manoeuvre_msg, uint64_t start_time);

// Close socket
USE_DLL int client_close();

// Client Get time
USE_DLL uint64_t client_get_time_ms();

#ifdef __cplusplus
}
#endif

#endif
