
/* ============================================================================

 UDP communication with limited packed size

 Author: Alessandro Mazzalai

 ============================================================================ */

#ifndef __UDP_LIMIT_FUNCTIONS_H
#define __UDP_LIMIT_FUNCTIONS_H

#if defined(_WIN32)
#pragma comment (lib, "Ws2_32.lib")
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Winsock2.h>
#include <Ws2tcpip.h>

#elif defined(__MACH__) || defined(__linux__)
#include <arpa/inet.h>
#include <sys/socket.h>
#endif

#include "UDP_limit_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

// Open the socket
int open_socket(const char *server_ip, int server_port);

// Create and set socket
int create_set_socket(application_type_t application_type, struct sockaddr_in *self_addr, struct sockaddr_in *target_addr);

// Close socket
int close_socket();

// Send message function
int send_message(volatile UDP_UINT server_run, UDP_UINT buffer_id, char *buffer, size_t buffer_size);

// Receive message function
int receive_message(volatile UDP_UINT *server_run, UDP_UINT *buffer_id, char *buffer, size_t buffer_size, uint64_t start_time);

// Get time function (milliseconds)
uint64_t get_time_ms();

// Sleep function (milliseconds)
void sleep_ms(unsigned int time_sleep_ms);



#ifdef __cplusplus
}
#endif

#endif
