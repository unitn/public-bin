/* ===============================================================================

  Client test for Server with co-driver for UDP communication

  Authors: Alessandro Mazzalai, Gastone Pietro Rosati Papini and Riccardo Don�

 =============================================================================== */

#if defined(_WIN32)
#pragma comment (lib, "Ws2_32.lib")
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <stdlib.h>
#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <direct.h>
#include <stdint.h>

#elif defined(__MACH__) || defined(__linux__)
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "codriver_client_functions.h"


#define DEFAULT_SERVER_IP  "127.0.0.1"
#define SERVER_PORT             30000  // Server port


int main(void)
{
	// Messages variables
	input_data_str scenario_msg = {}; //Inizialization to zeros
	output_data_str manoeuvre_msg = {}; //Inizialization to zeros
    uint32_t message_id = 0;

	// Init Client
	if (/* First Library function */client_init(DEFAULT_SERVER_IP,SERVER_PORT) == -1){
		return -1;
	}

	//uint64_t start_time, current_time;
	//uint64_t elapsed_time;
	//uint64_t codriver_timeout_ms = CODRIVER_TIMEOUT_MS; //it is a macro defined in UDP_limit_defines

	// First message --------------------------
    uint64_t start_time = /* Fifth Library function */client_get_time_ms();
    uint32_t server_run = 1;

	// This is are test numbers
	scenario_msg.ID = 32767;      //TEST NUMBER
	scenario_msg.CycleNumber = 1; //TEST NUMBER
	scenario_msg.VLgtFild = 20.0; //TEST NUMBER

	// Send and receive operations
	if (/* Second Library function */client_send(server_run, message_id, &scenario_msg) == -1) {
		fprintf(stderr, "Warning! Couldn't send the message with ID %d\n", message_id);
		exit(EXIT_FAILURE);
	} else {
		if (/* Third Library function */client_receive(&server_run, &message_id, &manoeuvre_msg, start_time) == -1) {
			fprintf(stderr, "Output message: manoeuvre not calculated\n");
		}
	}

	// If you get the same numbers the server void works
	printf("ID = %d\n", manoeuvre_msg.ID);                   //CHECK TEST NUMBER
	printf("CycleNumber = %d\n", manoeuvre_msg.CycleNumber); //CHECK TEST NUMBER
	printf("J0f = %f\n", manoeuvre_msg.J0f);                 //CHECK TEST NUMBER

	// Close socket
	if(/* Fourt Library function */client_close() == -1) {
		return -1;
	}
	printf("Done\n");
	return 0;
}
