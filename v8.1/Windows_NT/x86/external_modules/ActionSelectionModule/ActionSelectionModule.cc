#include "ActionSelectionModule.h"

#include "ActionSelectionModule.icc"

#include <math.h>
void ActionSelectionModule_TActionSelectionModuleInit ()
{
    int _D13_D4598_D4601;
    int _D12_D4597_D4603;
    int _D11_D4596_D4605;
    ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
    ActionSelectionModule_TMotorCortexHistory.SetShape(Max(5,0), Max(41,0), Max(
        41,0) );
    _D11_D4596_D4605 = 1;
    while (_D11_D4596_D4605 <= 5)
    {
        _D12_D4597_D4603 = 1;
        while (_D12_D4597_D4603 <= 41)
        {
            _D13_D4598_D4601 = 1;
            while (_D13_D4598_D4601 <= 41)
            {
                ActionSelectionModule_TMotorCortexHistory(_D11_D4596_D4605, 
                    _D12_D4597_D4603, _D13_D4598_D4601) = 0.;
                _D13_D4598_D4601 = _D13_D4598_D4601+1;
            }
            _D12_D4597_D4603 = _D12_D4597_D4603+1;
        }
        _D11_D4596_D4605 = _D11_D4596_D4605+1;
    }
    ActionSelectionModule_TMotorCortexHistorySize = 0;; 
}

doubleNN ActionSelectionModule_TBiasAndActionSelection ( const doubleNN &
    MotorCortex, const doubleNNN &BiasMatrices, const doubleN &SemanticCodes, 
    const doubleN &GuessWeights)
{
    doubleN Weights(0);
    doubleNN GainMatrix(0, 0);
    doubleNN InhibitionMatrix(0, 0);
    doubleNN MotorCortexOut(0, 0);
    int j_D4671_D4682;
    int i_D4670_D4684;
    int j_D4675_D4686;
    int i_D4674_D4688;
    int j_D4679_D4690;
    int i_D4678_D4692;




    Weights = GuessWeights;
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D4670_D4684 = 1;
    while (i_D4670_D4684 <= BiasMatrices.dimension(2))
    {
        j_D4671_D4682 = 1;
        while (j_D4671_D4682 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D4670_D4684+-1, 1+j_D4671_D4682+-1) = 1.+BiasMatrices
                (1, i_D4670_D4684, j_D4671_D4682)*Weights(1)+LightMax(
                ElemProduct (BiasMatrices(R(2,-1), i_D4670_D4684, j_D4671_D4682)
                , Weights(R(2,-1))));
            j_D4671_D4682 = j_D4671_D4682+1;
        }
        i_D4670_D4684 = i_D4670_D4684+1;
    }
    InhibitionMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(
        BiasMatrices.dimension(3)+-1+1,0) );
    i_D4674_D4688 = 1;
    while (i_D4674_D4688 <= BiasMatrices.dimension(2))
    {
        j_D4675_D4686 = 1;
        while (j_D4675_D4686 <= BiasMatrices.dimension(3))
        {
            InhibitionMatrix(1+i_D4674_D4688+-1, 1+j_D4675_D4686+-1) = 1.+-
                LightMax(ElemProduct (BiasMatrices(R(2,-1), i_D4674_D4688, 
                j_D4675_D4686), -Weights(R(2,-1))));
            j_D4675_D4686 = j_D4675_D4686+1;
        }
        i_D4674_D4688 = i_D4674_D4688+1;
    }
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D4678_D4692 = 1;
    while (i_D4678_D4692 <= BiasMatrices.dimension(2))
    {
        j_D4679_D4690 = 1;
        while (j_D4679_D4690 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D4678_D4692+-1, 1+j_D4679_D4690+-1) = GainMatrix(
                i_D4678_D4692, j_D4679_D4690)*Max(0.,InhibitionMatrix(
                i_D4678_D4692, j_D4679_D4690));
            j_D4679_D4690 = j_D4679_D4690+1;
        }
        i_D4678_D4692 = i_D4678_D4692+1;
    }
    return MotorCortexOut = ActionSelectionModule_TMSPRT (MotorCortex, 
        GainMatrix);
}

int ActionSelectionModule_TgetMotorCortexHistorySize ()
{
    return ActionSelectionModule_TMotorCortexHistorySize;
}

doubleNN ActionSelectionModule_TMSPRT ( const doubleNN &MotorCortex, const 
    doubleNN &GainMatrix)
{
    int i;
    int j;
    doubleNN MotorCortexHistoryMean(0, 0);
    doubleNN MotorCortexOut(0, 0);
    double threshold;
    int maxMotorCortexHistorySize;


    threshold = 6.9;
    maxMotorCortexHistorySize = ActionSelectionModule_TMotorCortexHistory
        .dimension(1);
    ActionSelectionModule_TMotorCortexHistorySize = Min(
        ActionSelectionModule_TMotorCortexHistorySize+1,
        maxMotorCortexHistorySize);
    ActionSelectionModule_TiMotorCortexHistoryLoop = Mod(
        ActionSelectionModule_TiMotorCortexHistoryLoop, 
        maxMotorCortexHistorySize)+1;
    ActionSelectionModule_TMotorCortexHistory.Set(
        ActionSelectionModule_TiMotorCortexHistoryLoop,All,All,MotorCortex);
    MotorCortexHistoryMean = light_total(
        ActionSelectionModule_TMotorCortexHistory(R(1,
        ActionSelectionModule_TMotorCortexHistorySize),All,All))/
        ActionSelectionModule_TMotorCortexHistorySize;
    MotorCortexOut = -(ElemProduct (GainMatrix, MotorCortexHistoryMean))+log(
        light_total(light_flatten(exp(ElemProduct (GainMatrix, 
        MotorCortexHistoryMean)))));
    if (LightMin(light_flatten(MotorCortexOut)) < threshold)
    {
        ActionSelectionModule_TMotorCortexHistorySize = 0;
        ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
        ActionSelectionModule_TMotorCortexHistory.Set(1,All,All,0.*
            MotorCortexHistoryMean);
    }
    return MotorCortexOut;
}

int ActionSelectionModule_TiMotorCortexHistoryLoop;
int ActionSelectionModule_TMotorCortexHistorySize;
doubleNNN ActionSelectionModule_TMotorCortexHistory(5, 41, 41);
