// File for interfaces
// WARNING! AUTOMATICALLY GENERATED - DO NOT EDIT
// Originf file: D4C_codriver_interfaces_v12.03.csv
// Origin CRC32: 1632199808

#ifndef codriver_interfaces_data_structs_h
#define codriver_interfaces_data_structs_h

#if defined(_DS1401)
#include "ds1401_defines.h"
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if defined(MATLAB_MEX_FILE) || defined(_DS1401)
typedef struct {
#elif defined(_WIN32)
#pragma pack(push, 1)
typedef struct {
#else
typedef struct __attribute__((packed)) {
#endif
  int32_t ID; /* Enumeration 
01=Scenario message
11=Manoeuvre message, */
  int32_t Version; /* Value is: 1201 */
  int32_t CycleNumber; /* This is an increasing number */
  double ECUupTime; /* Means system up-time */
  double AVItime; /* From DATALOG PC, CANape Multimedia1 signal */
  double TimeStamp; /* UTC time difference after 1st January 1970, obtained from GPS time with leap seconds (Unix epoch) */
  int32_t Status; /* 0 = ACTIVE, 0 != Fail  (means working correctly or not) */
  int32_t DrivingStyle; /* 0 = NOT_DEFINED, 1 = ECO, 2 = NORMAL, 3 = SPORT */
  int32_t ConfigParamInt2;
  int32_t ConfigParamInt3;
  int32_t ConfigParamInt4;
  int32_t ConfigParamInt5;
  double ConfigParamDouble1;
  double ConfigParamDouble2;
  double ConfigParamDouble3;
  double ConfigParamDouble4;
  double ConfigParamDouble5;
  double VLgtFild;
  double ALgtFild;
  double ALatFild;
  double YawRateFild; /* Note that yaw-rate is the derivative of the heading, i.e. chassis rotation rate, not speed rotation rate */
  double SteerWhlAg; /* Positive when the car is turning left */
  double SteerWhlAgSpd; /* Derivative of steering wheel angle */
  double SteerTorque;
  double EngineSpeed;
  double MasterCylinderPressure;
  double FuelConsumption;
  double GasPedPos;
  double EngineTorque;
  double EngineFrictionTorque;
  double MaxEngineTorque;
  double EngineTorqueDriverReq;
  double MaxEngineTorqueNorm;
  int32_t ExTemp;
  int32_t BrakePedalSwitchNCSts; /* 0 = UNKNOWN; 1 = RELEASED; 2 = PRESSED */
  int32_t ActGear;
  int32_t IndTurnComm; /* 0 = UNKNOWN; 1 = OFF; 2 = LEFT; 3 = RIGHT */
  int32_t VehicleID; /* 10 = CRF car, 20 = MIA, 30 = OpenDS, 40 = Carmaker */
  int32_t VehicleType; /* 1 = combustion engine, 2 = electric car */
  int32_t VehicleLightsStatus;
  double VehicleLen; /* Total length from front bumper to the rear bumper */
  double VehicleWidth;
  double VehicleBarLongPos; /* Distance from reference point to front bumper */
  double RequestedCruisingSpeed;
  int32_t AutomationLevel; /* 0 = NO AUTOMATION, 1 = ASSISTED, 2 = PARTIAL AUTOMATION, 3 = CONDITIONAL AUTOMATION, 4 = HIGH AUTOMATION, 5 = FULL AUTOMATION, 6 = UNKNOWN */
  int32_t SystemStatus;
  int32_t SystemMode;
  int32_t CurrentLane; /* Nomenclature from ADASIS: 0 = Unknown, 1 = Emergency lane, 2 = Single-lane road, 3 = Left-most lane, 4 = Right-most lane, 5 = One of middle lanes on road with three or more lanes */
  int32_t NrObjs; /* Limited to 20 max number of objects, selection needed (if more might be limited to nearest objects) */
  int32_t ObjID[20];
  int32_t ObjClass[20]; /* unknown(0), pedestrian(1), cyclist(2), moped(3), motorcycle(4), passengerCar(5), bus(6), lightTruck(7), heavyTruck(8), trailer(9), specialVehicles(10), tram(11), roadSideUnit(15) */
  int32_t ObjSensorInfo[20]; /* xxxxxxxD = LIDAR, xxxxxxDx = CAMERA, xxxxxDxx = RADAR, xxxxDxxx = V2V, xxxDxxxx = Blind Spot SX, xxDxxxxx = Blind Spot DX. */
  double ObjX[20]; /* Center of the object */
  double ObjY[20]; /* Centre of the object */
  double ObjLen[20]; /* Along object speed direction, along vehicle axis for stationary obstacles. 0 means unknown. */
  double ObjWidth[20]; /* Perpendicular to object speed direction, perpendicular to vehicle axis for stationary obstacles. 0 means unknown. */
  double ObjVel[20]; /* Speed module, not longitudinal speed */
  double ObjCourse[20]; /* In vehicle reference system, positive to the left */
  double ObjAcc[20]; /* Tangential acceleration */
  double ObjCourseRate[20];
  int32_t ObjNContourPoints[20]; /* Limited to 10 max number of contour points for each object */
  double ObjContourPoint1X[20]; /* In vehicle reference system */
  double ObjContourPoint1Y[20]; /* In vehicle reference system */
  double ObjContourPoint2X[20]; /* In vehicle reference system */
  double ObjContourPoint2Y[20]; /* In vehicle reference system */
  double ObjContourPoint3X[20]; /* In vehicle reference system */
  double ObjContourPoint3Y[20]; /* In vehicle reference system */
  double ObjContourPoint4X[20]; /* In vehicle reference system */
  double ObjContourPoint4Y[20]; /* In vehicle reference system */
  double ObjContourPoint5X[20]; /* In vehicle reference system */
  double ObjContourPoint5Y[20]; /* In vehicle reference system */
  double ObjContourPoint6X[20]; /* In vehicle reference system */
  double ObjContourPoint6Y[20]; /* In vehicle reference system */
  double ObjContourPoint7X[20]; /* In vehicle reference system */
  double ObjContourPoint7Y[20]; /* In vehicle reference system */
  double ObjContourPoint8X[20]; /* In vehicle reference system */
  double ObjContourPoint8Y[20]; /* In vehicle reference system */
  double ObjContourPoint9X[20]; /* In vehicle reference system */
  double ObjContourPoint9Y[20]; /* In vehicle reference system */
  double ObjContourPoint10X[20]; /* In vehicle reference system */
  double ObjContourPoint10Y[20]; /* In vehicle reference system */
  double LaneWidth;
  double LatOffsLineR; /* positive to the left */
  double LatOffsLineL;
  double LaneHeading;
  double LaneCrvt; /* Positive for left curves, current curvature (at the cars position) */
  double DetectionRange;
  int32_t LeftLineConf;
  int32_t RightLineConf;
  int32_t LeftLineType; /* 0 = dashed, 1 = solid, 2 = undecided, 3 = road edge, 4 = double lane, 5 = botts dots, 6 = not visible, 7 = invalid */
  int32_t RightLineType; /* 0 = dashed, 1 = solid, 2 = undecided, 3 = road edge, 4 = double lane, 5 = botts dots, 6 = not visible, 7 = invalid */
  double GpsLongitude; /* As measured by GPS, East positive */
  double GpsLatitude; /* As measured by GPS, North positive */
  double GpsSpeed; /* As measured by GPS */
  double GpsCourse; /* With respect to North, clockwise, as measured by GPS */
  double GpsHdop; /* Diluition of precision as indicated by GPS */
  double EgoLongitude; /* After position filter, referred to barycentre */
  double EgoLatitude; /* After position filter, referred to barycentre */
  double EgoCourse; /* With respect to North, clockwise, after position filter */
  double EgoDop; /* Diluition of precision, after position filter */
  int32_t FreeLaneLeft; /* 0 = NOT AVAILABLE; 1 = FREE; 2 = Unknown */
  int32_t FreeLaneRight; /* 0 = NOT AVAILABLE; 1 = FREE; 2 = Unknown */
  int32_t SideObstacleLeft; /* 0 = NO OBSTACLE: 1 = OBSTACLE PRESENT; 2 = Unknown */
  int32_t SideObstacleRight; /* 0 = NO OBSTACLE; 1 = OBSTACLE PRESENT; 2 = Unknown */
  int32_t BlindSpotObstacleLeft; /* 0 = NO OBSTACLE; 1 = OBSTACLE PRESENT; 2 = Unknown */
  int32_t BlindSpotObstacleRight; /* 0 = NO OBSTACLE; 1 = OBSTACLE PRESENT; 2 = Unknown */
  int32_t LeftAdjacentLane; /* 0 = NOT DETECTED; 1 = DETECTED; 2 = Unknown */
  int32_t RightAdjacentLane; /* 0 = NOT DETECTED; 1 = DETECTED; 2 = Unknown */
  int32_t NrPaths; /* Currently up to 5 paths can be transmitted, the first is the main path, others can be added at junctions (stubs) */
  int32_t NrLanesDrivingDirection; /* Considered at vehicle position */
  int32_t NrLanesOppositeDirection; /* Considered at vehicle position */
  int32_t AdasisCoordinatesNrP1; /* ADASIS description */
  int32_t AdasisCoordinatesNrP2; /* ADASIS description */
  int32_t AdasisCoordinatesNrP3; /* ADASIS description */
  int32_t AdasisCoordinatesNrP4; /* ADASIS description */
  int32_t AdasisCoordinatesNrP5; /* ADASIS description */
  double AdasisCoordinatesDist[100];
  double AdasisLongitudeValues[100]; /* For test purpose, only first point is necessary */
  double AdasisLatitudeValues[100]; /* For test purpose, only first point is necessary */
  int32_t AdasisHeadingChangeNrP1;
  int32_t AdasisHeadingChangeNrP2;
  int32_t AdasisHeadingChangeNrP3;
  int32_t AdasisHeadingChangeNrP4;
  int32_t AdasisHeadingChangeNrP5;
  double AdasisHeadingChangeDist[100];
  double AdasisHeadingChangeValues[100]; /* See definition in ADASIS protocol */
  int32_t AdasisCurvatureNrP1;
  int32_t AdasisCurvatureNrP2;
  int32_t AdasisCurvatureNrP3;
  int32_t AdasisCurvatureNrP4;
  int32_t AdasisCurvatureNrP5;
  double AdasisCurvatureDist[100];
  double AdasisCurvatureValues[100]; /* Positive for left curves */
  int32_t AdasisSpeedLimitNrP1;
  int32_t AdasisSpeedLimitNrP2;
  int32_t AdasisSpeedLimitNrP3;
  int32_t AdasisSpeedLimitNrP4;
  int32_t AdasisSpeedLimitNrP5;
  double AdasisSpeedLimitDist[10];
  int32_t AdasisSpeedLimitValues[10]; /* 0 means unknown */
  int32_t AdasisSlopeNrP1;
  int32_t AdasisSlopeNrP2;
  int32_t AdasisSlopeNrP3;
  int32_t AdasisSlopeNrP4;
  int32_t AdasisSlopeNrP5;
  double AdasisSlopeDist[100];
  double AdasisSlopeValues[100];
  int32_t AdasisNLanesNrP1;
  int32_t AdasisNLanesNrP2;
  int32_t AdasisNLanesNrP3;
  int32_t AdasisNLanesNrP4;
  int32_t AdasisNLanesNrP5;
  double AdasisNLanesDist[10];
  int32_t AdasisNLanesValues[10];
  int32_t AdasisLinkIdNrP1;
  int32_t AdasisLinkIdNrP2;
  int32_t AdasisLinkIdNrP3;
  int32_t AdasisLinkIdNrP4;
  int32_t AdasisLinkIdNrP5;
  double AdasisLinkIdDist[10];
  int32_t AdasisLinkIdValues[10];
  int32_t PriorityLevelNrP1;
  int32_t PriorityLevelNrP2;
  int32_t PriorityLevelNrP3;
  int32_t PriorityLevelNrP4;
  int32_t PriorityLevelNrP5;
  double PriorityLevelDist[10];
  int32_t PriorityLevelValues[10]; /* Not directly available from ADASIS, derived from other info */
  int32_t NrStubs; /* Up to 5 stubs can be described in the following list */
  double StubDist[5];
  int32_t ConnectingPathId[5]; /* Describes connection between main path and secondary path. 0 means connecting path is not identified */
  double TurnAngle[5];
  int32_t RightOfWay[5]; /* 0 = has priority, 1 = no priority, 2 = give priority, 3 = stop sign, 4 = unknown */
  int32_t StubNrLanesDrivingDirection[5];
  int32_t StubNrLanesOppositeDirection[5];
  int32_t NrLaneConnectivity; /* Up to 5 lane connectivity elements can be described in the following list */
  double LaneConnectivityDist[5];
  int32_t SuccLaneNr[5]; /* Lane number 1 always right */
  int32_t SuccLanePathId[5];
  int32_t FirstPredLaneNr[5];
  double FirstPredLaneSideOffset[5]; /* Positive for successor lane on the left of predecessor lane, 0 means in line */
  int32_t LastPredLaneNr[5];
  int32_t LastPredLaneSideOffset[5]; /* Positive for successor lane on the left of predecessor lane, 0 means in line */
  int32_t PredLanesPathId[5];
  int32_t NrTrfLights; /* Only first traffic ligh is described if available */
  double TrfLightDist;
  int32_t TrfLightCurrState; /* 1 = Green, 2 = Yellow, 3 = Red, 0 = Flashing */
  double TrfLightFirstTimeToChange;
  int32_t TrfLightFirstNextState; /* 1 = Green, 2 = Yellow, 3 = Red, 0 = Flashing */
  double TrfLightSecondTimeToChange;
  int32_t TrfLightSecondNextState; /* 1 = Green, 2 = Yellow, 3 = Red, 0 = Flashing */
  int32_t NrPedCross; /* Only first pedestrian crossing is described if available */
  double PedCrossDist;
} input_data_str;
#if defined(MATLAB_MEX_FILE) || defined(_DS1401)
// Do nothing
#elif defined(_WIN32)
#pragma pack(pop)
#endif

#if defined(MATLAB_MEX_FILE) || defined(_DS1401)
typedef struct {
#elif defined(_WIN32)
#pragma pack(push, 1)
typedef struct {
#else
typedef struct __attribute__((packed)) {
#endif
  int32_t ID; /* Enumeration 
01=Scenario message
11=Manoeuvre message, */
  int32_t Version; /* Value is: 1201 */
  int32_t CycleNumber; /* This is an increasing number */
  double ECUupTime; /* Means system up-time */
  double AVItime; /* From DATALOG PC, CANape Multimedia1 signal */
  double TimeStamp; /* UTC time difference after 1st January 1970, obtained from GPS time with leap seconds (Unix epoch) */
  int32_t Status; /* 0 = ACTIVE, 0 != Fail  (means working correctly or not) */
  int32_t CoDriverVersion;
  int32_t CoDriverParamInt2;
  int32_t CoDriverParamInt3;
  int32_t CoDriverParamInt4;
  int32_t CoDriverParamInt5;
  double CoDriverParamDouble1;
  double CoDriverParamDouble2;
  double CoDriverParamDouble3;
  double CoDriverParamDouble4;
  double CoDriverParamDouble5;
  int32_t TimeHeadwayPolicyf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LegalSpeedPolicyf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LegalSpeedLimitf;
  int32_t LandmarkPolicyf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LandmarkTypef; /* 0 = NO LANDMARK
1 = STOP SIGN
2 = PEDESTRIAN CROSSING
3 = YIELD SIGN
4 = SEMAPHORE */
  int32_t AccelerationPolicyForCurvef; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW LEFT
3 = YELLOW RIGHT
4 = RED LEFT
5 = RED RIGHT */
  int32_t RearTimeHeadwayPolicyLeftf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LeftThreatTypef; /* 0 = SIDE OBSTACLE; 1 = RUN OFF ROAD */
  int32_t RearTimeHeadwayPolicyRightf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t RightThreatTypef; /* 0 = SIDE OBSTACLE; 1 = RUN OFF ROAD */
  int32_t LeftLanePolicyf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t RightLanePolicyf; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t TravelTimePolicyf; /* 0 = NOT COMPUTED
1 = COMFORT
2 = NORMAL
3 = SPORT */
  int32_t RecommendedGearf; /* 0 = NOT COMPUTED */
  int32_t TimeHeadwayPolicys; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LegalSpeedPolicys; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LegalSpeedLimits;
  int32_t LandmarkPolicys; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LandmarkTypes; /* 0 = NO LANDMARK
1 = STOP SIGN
2 = PEDESTRIAN CROSSING
3 = YIELD SIGN
4 = SEMAPHORE */
  int32_t AccelerationPolicyForCurves; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW LEFT
3 = YELLOW RIGHT
4 = RED LEFT
5 = RED RIGHT */
  int32_t RearTimeHeadwayPolicyLefts; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t LeftThreatTypes; /* 0 = SIDE OBSTACLE; 1 = RUN OFF ROAD */
  int32_t RearTimeHeadwayPolicyRights; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t RightThreatTypes; /* 0 = SIDE OBSTACLE; 1 = RUN OFF ROAD */
  int32_t LeftLanePolicys; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t RightLanePolicys; /* 0 = NOT COMPUTED
1 = GREEN
2 = YELLOW
3 = RED */
  int32_t TravelTimePolicys; /* 0 = NOT COMPUTED
1 = COMFORT
2 = NORMAL
3 = SPORT */
  int32_t RecommendedGears; /* 0 = NOT COMPUTED */
  int32_t TargetIDf;
  int32_t TargetClassf; /* 0 = UNCLASSIFIED
1 = UNKNOWN SMALL
2 = UNKNOWN BIG
3 = PEDESTRIAN
4 = BIKE OR MOTORBIKE
5 = CAR
6 = TRUCK OR BUS */
  int32_t TargetSensorInformationf; /* xxxxxxxD = LIDAR
xxxxxxDx = CAMERA
xxxxxDxx = RADAR
xxxxDxxx = V2V */
  double TargetXf;
  double TargetYf;
  double TargetDistancef; /* Distance to intersection obtained from absolute positions of vehicle and target */
  double TargetLengthf;
  double TargetWidthf;
  double TargetSpeedf; /* 0 means stationary */
  double TargetCoursef;
  double TargetAccelerationf;
  double TargetCourseRatef;
  int32_t TargetDrivingModef; /* 0 = NO AUTOMATION, 1 = ASSISTED, 2 = PARTIAL AUTOMATION, 3 = CONDITIONAL AUTOMATION, 4 = HIGH AUTOMATION, 5 = FULL AUTOMATION, 6 = UNKNOWN */
  int32_t TargetIDs;
  int32_t TargetClasss; /* 0 = UNCLASSIFIED
1 = UNKNOWN SMALL
2 = UNKNOWN BIG
3 = PEDESTRIAN
4 = BIKE OR MOTORBIKE
5 = CAR
6 = TRUCK OR BUS */
  int32_t TargetSensorInformations; /* xxxxxxxD = LIDAR
xxxxxxDx = CAMERA
xxxxxDxx = RADAR
xxxxDxxx = V2V */
  double TargetXs;
  double TargetYs;
  double TargetDistances; /* Distance to intersection obtained from absolute positions of vehicle and target */
  double TargetLengths;
  double TargetWidths;
  double TargetSpeeds; /* 0 means stationary */
  double TargetCourses;
  double TargetAccelerations;
  double TargetCourseRates;
  int32_t TargetDrivingModes; /* 0 = NO AUTOMATION, 1 = ASSISTED, 2 = PARTIAL AUTOMATION, 3 = CONDITIONAL AUTOMATION, 4 = HIGH AUTOMATION, 5 = FULL AUTOMATION, 6 = UNKNOWN */
  int32_t NTrajectoryPointsf; /* Limited to 23 max number of trajectory points */
  double TrajectoryPointITimef[23]; /* Unix epoch */
  double TrajectoryPointIXf[23]; /* In vehicle reference system */
  double TrajectoryPointIYf[23]; /* In vehicle reference system */
  int32_t NTrajectoryPointss; /* Limited to 23 max number of trajectory points */
  double TrajectoryPointITimes[23]; /* Unix epoch */
  double TrajectoryPointIXs[23]; /* In vehicle reference system */
  double TrajectoryPointIYs[23]; /* In vehicle reference system */
  double T0; /* ECU up time when the primitive starts (based on ECUs given by Scenario Messages) */
  double V0; /* Longitudinal speed at the time of generation of the motor primitive */
  double A0; /* Time derivative of speed, also valid for second trajectory */
  double T1f; /* Relative to T0 */
  double J0f; /* Time derivative of acceleration */
  double S0f; /* Time derivative of jerk */
  double Cr0f; /* Time derivative of snap */
  double T2f; /* Relative to T0 */
  double J1f; /* Time derivative of acceleration */
  double S1f; /* Time derivative of jerk */
  double Cr1f; /* Time derivative of snap */
  double Sn0; /* Also valid for second trajectory */
  double Alpha0; /* Also valid for second trajectory */
  double Delta0; /* Curvature of vehicle trajectory relative to lane curvature */
  double T1nf;
  double Jdelta0f;
  double Sdelta0f;
  double Crdelta0f;
  double T2nf;
  double Jdelta1f;
  double Sdelta1f;
  double Crdelta1f;
  double T1s;
  double J0s; /* Time derivative of acceleration */
  double S0s; /* Time derivative of jerk */
  double Cr0s; /* Time derivative of snap */
  double T2s;
  double J1s; /* Time derivative of acceleration */
  double S1s; /* Time derivative of jerk */
  double Cr1s; /* Time derivative of snap */
  double T1ns;
  double Jdelta0s;
  double Sdelta0s;
  double Crdelta0s;
  double T2ns;
  double Jdelta1s;
  double Sdelta1s;
  double Crdelta1s;
  int32_t FirstManoeuverTypeLong; /* E.g: follow object, free flow, stopping, etc. */
  int32_t FirstManoeuverTypeLat; /* E.g: lane keeping, lane change left, lane change right, etc. */
  int32_t SecondManoeuverTypeLong; /* E.g: follow object, free flow, stopping, etc. */
  int32_t SecondManoeuverTypeLat; /* E.g: lane keeping, lane change left, lane change right, etc. */
  double TargetSpeed; /* Speed of the vehicle at the end of the manoeuvre */
  double TargetLongitudinalAcceleration; /* Longitudinal acceleration required to perform the calculated manoeuvre */
  double TargetDistanceToPreceedingVehicle; /* Distance from the preceding vehicle at the end of the manoeuvre */
} output_data_str;
#if defined(MATLAB_MEX_FILE) || defined(_DS1401)
// Do nothing
#elif defined(_WIN32)
#pragma pack(pop)
#endif


// Scenario message union
typedef union {
  input_data_str data_struct;
  char data_buffer[sizeof(input_data_str)];
} scenario_msg_t;

// Manoeuvre message union
typedef union {
  output_data_str data_struct;
  char data_buffer[sizeof(output_data_str)];
} manoeuvre_msg_t;

#ifdef __cplusplus
} // extern "C"
#endif

#endif
