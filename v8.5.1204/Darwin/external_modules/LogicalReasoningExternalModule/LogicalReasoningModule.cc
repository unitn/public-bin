#include "LogicalReasoningModule.h"

#include "LogicalReasoningModule.icc"

#include <math.h>
doubleNN LogicalReasoningModule_TLogicalReasoningModule ( const int &ID, const 
    double &AVItime, const double &ALgtFild, const double &VLgtFild, const int &
    CurrentLane, const double &LaneWidth, const int &NrObjs, const doubleN &
    ObjFound, const doubleN &ObjS0, const doubleN &ObjN0, const doubleN &ObjB0, 
    const doubleN &ObjV0, const doubleN &ObjA0, const doubleN &ObjCourse, const 
    doubleN &ObjLen, const doubleN &ObjWidth, const doubleN &ObjLane, const int 
    &NrStubs, const doubleN &StubDist, const intN &ConnectingPathId, const 
    doubleN &TurnAngle, const intN &RightOfWay, const intN &
    StubNrLanesDrivingDirection, const intN &StubNrLanesOppositeDirection, 
    const int &NrLaneConnectivity, const doubleN &LaneConnectivityDist, const 
    intN &SuccLaneNr, const intN &SuccLanePathid, const int &FirstPredLaneNr, 
    const doubleN &FirstPredLaneSideOffset, const int &LastPredLaneNr, const 
    doubleN &LastPredLaneSideOffset, const intN &PredLanesPathid, const int &
    NrTrfLights, const doubleN &TrfLightDist, const intN &TrfLightCurrState, 
    const doubleN &TrfLightFirstTimeToChange, const intN &TrfLightSecondState, 
    const doubleN &TrfLightSecondTimeToChange, const intN &TrfLightThirdState, 
    const doubleN &TrfLightThirdTimeToChange, const int &NrPedCross, const 
    doubleN &PedCrossDist)
{
    int NrBoundingBoxes;
    doubleNN BoundingBoxes(0, 0);
    int i;
    int j;
    int j_D212681_D212684;
    int i_D212680_D212686;

    BoundingBoxes.SetShape(Max(10,0), Max(7,0) );
    i_D212680_D212686 = 1;
    while (i_D212680_D212686 <= 10)
    {
        j_D212681_D212684 = 1;
        while (j_D212681_D212684 <= 7)
        {
            BoundingBoxes(1+i_D212680_D212686+-1, 1+j_D212681_D212684+-1) = 0.;
            j_D212681_D212684 = j_D212681_D212684+1;
        }
        i_D212680_D212686 = i_D212680_D212686+1;
    }
    NrBoundingBoxes = 0;
    i = 1;
    while (i <= NrBoundingBoxes)
    {
        BoundingBoxes(i, 1) = 2.;
        BoundingBoxes(i, 2) = 0.;
        BoundingBoxes(i, 3) = 0.;
        BoundingBoxes(i, 4) = 0.;
        BoundingBoxes(i, 5) = 0.;
        BoundingBoxes(i, 6) = 0.;
        BoundingBoxes(i, 7) = 0.5;
        i = i+1;
    }
    return BoundingBoxes(R(1,NrBoundingBoxes),All);
}

void LogicalReasoningModule_TLogicalReasoningModuleInit ()
{
; 
}

