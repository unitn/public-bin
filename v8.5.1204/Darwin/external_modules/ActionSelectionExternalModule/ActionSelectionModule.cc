#include "ActionSelectionModule.h"

#include "ActionSelectionModule.icc"

#include <math.h>
void ActionSelectionModule_TActionSelectionModuleInit ()
{
    int _D5_D211838_D211841;
    int _D4_D211837_D211843;
    int _D3_D211836_D211845;
    ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
    ActionSelectionModule_TMotorCortexHistory.SetShape(Max(5,0), Max(41,0), Max(
        41,0) );
    _D3_D211836_D211845 = 1;
    while (_D3_D211836_D211845 <= 5)
    {
        _D4_D211837_D211843 = 1;
        while (_D4_D211837_D211843 <= 41)
        {
            _D5_D211838_D211841 = 1;
            while (_D5_D211838_D211841 <= 41)
            {
                ActionSelectionModule_TMotorCortexHistory(_D3_D211836_D211845, 
                    _D4_D211837_D211843, _D5_D211838_D211841) = 0.;
                _D5_D211838_D211841 = _D5_D211838_D211841+1;
            }
            _D4_D211837_D211843 = _D4_D211837_D211843+1;
        }
        _D3_D211836_D211845 = _D3_D211836_D211845+1;
    }
    ActionSelectionModule_TMotorCortexHistorySize = 0;; 
}

doubleNN ActionSelectionModule_TBiasAndActionSelection ( const doubleNN &
    MotorCortex, const doubleNNN &BiasMatrices, const doubleN &SemanticCodes, 
    const doubleN &GuessWeights)
{
    doubleN Weights(0);
    doubleNN GainMatrix(0, 0);
    doubleNN InhibitionMatrix(0, 0);
    doubleNN MotorCortexOut(0, 0);
    int j_D211911_D211922;
    int i_D211910_D211924;
    int j_D211915_D211926;
    int i_D211914_D211928;
    int j_D211919_D211930;
    int i_D211918_D211932;




    Weights = GuessWeights;
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D211910_D211924 = 1;
    while (i_D211910_D211924 <= BiasMatrices.dimension(2))
    {
        j_D211911_D211922 = 1;
        while (j_D211911_D211922 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D211910_D211924+-1, 1+j_D211911_D211922+-1) = 1.+
                BiasMatrices(1, i_D211910_D211924, j_D211911_D211922)*Weights(1)
                +LightMax(ElemProduct (BiasMatrices(R(2,-1), i_D211910_D211924, 
                j_D211911_D211922), Weights(R(2,-1))));
            j_D211911_D211922 = j_D211911_D211922+1;
        }
        i_D211910_D211924 = i_D211910_D211924+1;
    }
    InhibitionMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(
        BiasMatrices.dimension(3)+-1+1,0) );
    i_D211914_D211928 = 1;
    while (i_D211914_D211928 <= BiasMatrices.dimension(2))
    {
        j_D211915_D211926 = 1;
        while (j_D211915_D211926 <= BiasMatrices.dimension(3))
        {
            InhibitionMatrix(1+i_D211914_D211928+-1, 1+j_D211915_D211926+-1) = 
                1.+-LightMax(ElemProduct (BiasMatrices(R(2,-1), 
                i_D211914_D211928, j_D211915_D211926), -Weights(R(2,-1))));
            j_D211915_D211926 = j_D211915_D211926+1;
        }
        i_D211914_D211928 = i_D211914_D211928+1;
    }
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D211918_D211932 = 1;
    while (i_D211918_D211932 <= BiasMatrices.dimension(2))
    {
        j_D211919_D211930 = 1;
        while (j_D211919_D211930 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D211918_D211932+-1, 1+j_D211919_D211930+-1) = 
                GainMatrix(i_D211918_D211932, j_D211919_D211930)*Max(0.,
                InhibitionMatrix(i_D211918_D211932, j_D211919_D211930));
            j_D211919_D211930 = j_D211919_D211930+1;
        }
        i_D211918_D211932 = i_D211918_D211932+1;
    }
    return MotorCortexOut = ActionSelectionModule_TMSPRT (MotorCortex, 
        GainMatrix);
}

int ActionSelectionModule_TgetMotorCortexHistorySize ()
{
    return ActionSelectionModule_TMotorCortexHistorySize;
}

doubleNN ActionSelectionModule_TMSPRT ( const doubleNN &MotorCortex, const 
    doubleNN &GainMatrix)
{
    int i;
    int j;
    doubleNN MotorCortexHistoryMean(0, 0);
    doubleNN MotorCortexOut(0, 0);
    double threshold;
    int maxMotorCortexHistorySize;


    threshold = 6.9;
    maxMotorCortexHistorySize = ActionSelectionModule_TMotorCortexHistory
        .dimension(1);
    ActionSelectionModule_TMotorCortexHistorySize = Min(
        ActionSelectionModule_TMotorCortexHistorySize+1,
        maxMotorCortexHistorySize);
    ActionSelectionModule_TiMotorCortexHistoryLoop = Mod(
        ActionSelectionModule_TiMotorCortexHistoryLoop, 
        maxMotorCortexHistorySize)+1;
    ActionSelectionModule_TMotorCortexHistory.Set(
        ActionSelectionModule_TiMotorCortexHistoryLoop,All,All,MotorCortex);
    MotorCortexHistoryMean = light_total(
        ActionSelectionModule_TMotorCortexHistory(R(1,
        ActionSelectionModule_TMotorCortexHistorySize),All,All))/
        ActionSelectionModule_TMotorCortexHistorySize;
    MotorCortexOut = -(ElemProduct (GainMatrix, MotorCortexHistoryMean))+log(
        light_total(light_flatten(exp(ElemProduct (GainMatrix, 
        MotorCortexHistoryMean)))));
    if (LightMin(light_flatten(MotorCortexOut)) < threshold)
    {
        ActionSelectionModule_TMotorCortexHistorySize = 0;
        ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
        ActionSelectionModule_TMotorCortexHistory.Set(1,All,All,0.*
            MotorCortexHistoryMean);
    }
    return MotorCortexOut;
}

int ActionSelectionModule_TiMotorCortexHistoryLoop;
int ActionSelectionModule_TMotorCortexHistorySize;
doubleNNN ActionSelectionModule_TMotorCortexHistory(5, 41, 41);
