#ifndef ActionSelectionModule_h
#define ActionSelectionModule_h

#define LM_NNNN
/* Level of the library */
#include "lightmat.h"

void ActionSelectionModule_TActionSelectionModuleInit ();

doubleNN ActionSelectionModule_TBiasAndActionSelection ( const doubleNN &, 
    const doubleNNN &, const doubleN &, const doubleN &);

int ActionSelectionModule_TgetMotorCortexHistorySize ();

doubleNN ActionSelectionModule_TMSPRT ( const doubleNN &, const doubleNN &);

extern int ActionSelectionModule_TiMotorCortexHistoryLoop;
extern int ActionSelectionModule_TMotorCortexHistorySize;
extern doubleNNN ActionSelectionModule_TMotorCortexHistory;
#endif
