#include "ActionSelectionModule.h"

#include "ActionSelectionModule.icc"

#include <math.h>
void ActionSelectionModule_TActionSelectionModuleInit ()
{
    int _D13_D3961_D3964;
    int _D12_D3960_D3966;
    int _D11_D3959_D3968;
    ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
    ActionSelectionModule_TMotorCortexHistory.SetShape(Max(5,0), Max(41,0), Max(
        41,0) );
    _D11_D3959_D3968 = 1;
    while (_D11_D3959_D3968 <= 5)
    {
        _D12_D3960_D3966 = 1;
        while (_D12_D3960_D3966 <= 41)
        {
            _D13_D3961_D3964 = 1;
            while (_D13_D3961_D3964 <= 41)
            {
                ActionSelectionModule_TMotorCortexHistory(_D11_D3959_D3968, 
                    _D12_D3960_D3966, _D13_D3961_D3964) = 0.;
                _D13_D3961_D3964 = _D13_D3961_D3964+1;
            }
            _D12_D3960_D3966 = _D12_D3960_D3966+1;
        }
        _D11_D3959_D3968 = _D11_D3959_D3968+1;
    }
    ActionSelectionModule_TMotorCortexHistorySize = 0;; 
}

doubleNN ActionSelectionModule_TBiasAndActionSelection ( const doubleNN &
    MotorCortex, const doubleNNN &BiasMatrices, const doubleN &SemanticCodes, 
    const doubleN &GuessWeights)
{
    doubleN Weights(0);
    doubleNN GainMatrix(0, 0);
    doubleNN InhibitionMatrix(0, 0);
    doubleNN MotorCortexOut(0, 0);
    int j_D4034_D4045;
    int i_D4033_D4047;
    int j_D4038_D4049;
    int i_D4037_D4051;
    int j_D4042_D4053;
    int i_D4041_D4055;




    Weights = GuessWeights;
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D4033_D4047 = 1;
    while (i_D4033_D4047 <= BiasMatrices.dimension(2))
    {
        j_D4034_D4045 = 1;
        while (j_D4034_D4045 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D4033_D4047+-1, 1+j_D4034_D4045+-1) = 1.+BiasMatrices
                (1, i_D4033_D4047, j_D4034_D4045)*Weights(1)+LightMax(
                ElemProduct (BiasMatrices(R(2,-1), i_D4033_D4047, j_D4034_D4045)
                , Weights(R(2,-1))));
            j_D4034_D4045 = j_D4034_D4045+1;
        }
        i_D4033_D4047 = i_D4033_D4047+1;
    }
    InhibitionMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(
        BiasMatrices.dimension(3)+-1+1,0) );
    i_D4037_D4051 = 1;
    while (i_D4037_D4051 <= BiasMatrices.dimension(2))
    {
        j_D4038_D4049 = 1;
        while (j_D4038_D4049 <= BiasMatrices.dimension(3))
        {
            InhibitionMatrix(1+i_D4037_D4051+-1, 1+j_D4038_D4049+-1) = 1.+-
                LightMax(ElemProduct (BiasMatrices(R(2,-1), i_D4037_D4051, 
                j_D4038_D4049), -Weights(R(2,-1))));
            j_D4038_D4049 = j_D4038_D4049+1;
        }
        i_D4037_D4051 = i_D4037_D4051+1;
    }
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D4041_D4055 = 1;
    while (i_D4041_D4055 <= BiasMatrices.dimension(2))
    {
        j_D4042_D4053 = 1;
        while (j_D4042_D4053 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D4041_D4055+-1, 1+j_D4042_D4053+-1) = GainMatrix(
                i_D4041_D4055, j_D4042_D4053)*Max(0.,InhibitionMatrix(
                i_D4041_D4055, j_D4042_D4053));
            j_D4042_D4053 = j_D4042_D4053+1;
        }
        i_D4041_D4055 = i_D4041_D4055+1;
    }
    return MotorCortexOut = ActionSelectionModule_TMSPRT (MotorCortex, 
        GainMatrix);
}

int ActionSelectionModule_TgetMotorCortexHistorySize ()
{
    return ActionSelectionModule_TMotorCortexHistorySize;
}

doubleNN ActionSelectionModule_TMSPRT ( const doubleNN &MotorCortex, const 
    doubleNN &GainMatrix)
{
    int i;
    int j;
    doubleNN MotorCortexHistoryMean(0, 0);
    doubleNN MotorCortexOut(0, 0);
    double threshold;
    int maxMotorCortexHistorySize;


    threshold = 6.9;
    maxMotorCortexHistorySize = ActionSelectionModule_TMotorCortexHistory
        .dimension(1);
    ActionSelectionModule_TMotorCortexHistorySize = Min(
        ActionSelectionModule_TMotorCortexHistorySize+1,
        maxMotorCortexHistorySize);
    ActionSelectionModule_TiMotorCortexHistoryLoop = Mod(
        ActionSelectionModule_TiMotorCortexHistoryLoop, 
        maxMotorCortexHistorySize)+1;
    ActionSelectionModule_TMotorCortexHistory.Set(
        ActionSelectionModule_TiMotorCortexHistoryLoop,All,All,MotorCortex);
    MotorCortexHistoryMean = light_total(
        ActionSelectionModule_TMotorCortexHistory(R(1,
        ActionSelectionModule_TMotorCortexHistorySize),All,All))/
        ActionSelectionModule_TMotorCortexHistorySize;
    MotorCortexOut = -(ElemProduct (GainMatrix, MotorCortexHistoryMean))+log(
        light_total(light_flatten(exp(ElemProduct (GainMatrix, 
        MotorCortexHistoryMean)))));
    if (LightMin(light_flatten(MotorCortexOut)) < threshold)
    {
        ActionSelectionModule_TMotorCortexHistorySize = 0;
        ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
        ActionSelectionModule_TMotorCortexHistory.Set(1,All,All,0.*
            MotorCortexHistoryMean);
    }
    return MotorCortexOut;
}

int ActionSelectionModule_TiMotorCortexHistoryLoop;
int ActionSelectionModule_TMotorCortexHistorySize;
doubleNNN ActionSelectionModule_TMotorCortexHistory(5, 41, 41);
