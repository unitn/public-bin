#include "LogicalReasoningExternalModule.h"

#include <math.h>


doubleNN LogicalReasoningModule ( const int &ID, const 
    double &AVItime, const double &ALgtFild, const double &VLgtFild, const int &
    CurrentLane, const double &LaneWidth, const int &NrObjs, const doubleN &
    ObjFound, const doubleN &ObjS0, const doubleN &ObjN0, const doubleN &ObjB0, 
    const doubleN &ObjV0, const doubleN &ObjA0, const doubleN &ObjCourse, const 
    doubleN &ObjLen, const doubleN &ObjWidth, const doubleN &ObjLane, const int 
    &NrStubs, const doubleN &StubDist, const intN &ConnectingPathId, const 
    doubleN &TurnAngle, const intN &RightOfWay, const intN &
    StubNrLanesDrivingDirection, const intN &StubNrLanesOppositeDirection, 
    const int &NrLaneConnectivity, const doubleN &LaneConnectivityDist, const 
    intN &SuccLaneNr, const intN &SuccLanePathid, const int &FirstPredLaneNr, 
    const doubleN &FirstPredLaneSideOffset, const int &LastPredLaneNr, const 
    doubleN &LastPredLaneSideOffset, const intN &PredLanesPathid, const int &
    NrTrfLights, const doubleN &TrfLightDist, const intN &TrfLightCurrState, 
    const doubleN &TrfLightFirstTimeToChange, const intN &TrfLightSecondState, 
    const doubleN &TrfLightSecondTimeToChange, const intN &TrfLightThirdState, 
    const doubleN &TrfLightThirdTimeToChange, const int &NrPedCross, const 
    doubleN &PedCrossDist)
{
    int NrBoundingBoxes;
    doubleNN BoundingBoxes(0, 0);
    int i;
    int j;

	/*maximum number of Bounding Boxes*/
    BoundingBoxes.SetShape(Max(10,0), Max(7,0) );
    i = 1;
    while (i <= 10)
    {
        j = 1;
        while (j <= 7)
        {
            BoundingBoxes(1+i+-1, 1+j+-1) = 0.;
            j = j+1;
        }
        i = i+1;
    }

	/*The LRM computation stays here*/

	/*The output of the LRM must fill the BoundingBoxes array*/
    NrBoundingBoxes = 0;
    i = 1;
    while (i <= NrBoundingBoxes)
    {
        BoundingBoxes(i, 1) = 2.;  /*1: time ahead*/
        BoundingBoxes(i, 2) = 0.;  /*2: x left bottom corner*/
        BoundingBoxes(i, 3) = 0.;  /*3: y left bottom corner*/
        BoundingBoxes(i, 4) = 0.;  /*4: box width*/
        BoundingBoxes(i, 5) = 0.;  /*5: box height*/
        BoundingBoxes(i, 6) = 0.;  /*6: box salience 0-2 (1 is neutral)*/
        BoundingBoxes(i, 7) = 0.5; /*7: SemanticCode*/
        i = i+1;
    }
    return BoundingBoxes(R(1,NrBoundingBoxes),All); /*Trim BoundingBoxes Array*/
}

void LogicalReasoningModuleInit ()
{
; 
}

