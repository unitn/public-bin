#include "ActionSelectionModule.h"

#include "ActionSelectionModule.icc"

#include <math.h>
void ActionSelectionModule_TActionSelectionModuleInit ()
{
    int _D13_D4608_D4611;
    int _D12_D4607_D4613;
    int _D11_D4606_D4615;
    ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
    ActionSelectionModule_TMotorCortexHistory.SetShape(Max(5,0), Max(41,0), Max(
        41,0) );
    _D11_D4606_D4615 = 1;
    while (_D11_D4606_D4615 <= 5)
    {
        _D12_D4607_D4613 = 1;
        while (_D12_D4607_D4613 <= 41)
        {
            _D13_D4608_D4611 = 1;
            while (_D13_D4608_D4611 <= 41)
            {
                ActionSelectionModule_TMotorCortexHistory(_D11_D4606_D4615, 
                    _D12_D4607_D4613, _D13_D4608_D4611) = 0.;
                _D13_D4608_D4611 = _D13_D4608_D4611+1;
            }
            _D12_D4607_D4613 = _D12_D4607_D4613+1;
        }
        _D11_D4606_D4615 = _D11_D4606_D4615+1;
    }
    ActionSelectionModule_TMotorCortexHistorySize = 0;; 
}

doubleNN ActionSelectionModule_TBiasAndActionSelection ( const doubleNN &
    MotorCortex, const doubleNNN &BiasMatrices, const doubleN &SemanticCodes, 
    const doubleN &GuessWeights)
{
    doubleN Weights(0);
    doubleNN GainMatrix(0, 0);
    doubleNN InhibitionMatrix(0, 0);
    doubleNN MotorCortexOut(0, 0);
    int j_D4681_D4692;
    int i_D4680_D4694;
    int j_D4685_D4696;
    int i_D4684_D4698;
    int j_D4689_D4700;
    int i_D4688_D4702;




    Weights = GuessWeights;
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D4680_D4694 = 1;
    while (i_D4680_D4694 <= BiasMatrices.dimension(2))
    {
        j_D4681_D4692 = 1;
        while (j_D4681_D4692 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D4680_D4694+-1, 1+j_D4681_D4692+-1) = 1.+BiasMatrices
                (1, i_D4680_D4694, j_D4681_D4692)*Weights(1)+LightMax(
                ElemProduct (BiasMatrices(R(2,-1), i_D4680_D4694, j_D4681_D4692)
                , Weights(R(2,-1))));
            j_D4681_D4692 = j_D4681_D4692+1;
        }
        i_D4680_D4694 = i_D4680_D4694+1;
    }
    InhibitionMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(
        BiasMatrices.dimension(3)+-1+1,0) );
    i_D4684_D4698 = 1;
    while (i_D4684_D4698 <= BiasMatrices.dimension(2))
    {
        j_D4685_D4696 = 1;
        while (j_D4685_D4696 <= BiasMatrices.dimension(3))
        {
            InhibitionMatrix(1+i_D4684_D4698+-1, 1+j_D4685_D4696+-1) = 1.+-
                LightMax(ElemProduct (BiasMatrices(R(2,-1), i_D4684_D4698, 
                j_D4685_D4696), -Weights(R(2,-1))));
            j_D4685_D4696 = j_D4685_D4696+1;
        }
        i_D4684_D4698 = i_D4684_D4698+1;
    }
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2)+-1+1,0), Max(BiasMatrices
        .dimension(3)+-1+1,0) );
    i_D4688_D4702 = 1;
    while (i_D4688_D4702 <= BiasMatrices.dimension(2))
    {
        j_D4689_D4700 = 1;
        while (j_D4689_D4700 <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i_D4688_D4702+-1, 1+j_D4689_D4700+-1) = GainMatrix(
                i_D4688_D4702, j_D4689_D4700)*Max(0.,InhibitionMatrix(
                i_D4688_D4702, j_D4689_D4700));
            j_D4689_D4700 = j_D4689_D4700+1;
        }
        i_D4688_D4702 = i_D4688_D4702+1;
    }
    return MotorCortexOut = ActionSelectionModule_TMSPRT (MotorCortex, 
        GainMatrix);
}

int ActionSelectionModule_TgetMotorCortexHistorySize ()
{
    return ActionSelectionModule_TMotorCortexHistorySize;
}

doubleNN ActionSelectionModule_TMSPRT ( const doubleNN &MotorCortex, const 
    doubleNN &GainMatrix)
{
    int i;
    int j;
    doubleNN MotorCortexHistoryMean(0, 0);
    doubleNN MotorCortexOut(0, 0);
    double threshold;
    int maxMotorCortexHistorySize;


    threshold = 6.9;
    maxMotorCortexHistorySize = ActionSelectionModule_TMotorCortexHistory
        .dimension(1);
    ActionSelectionModule_TMotorCortexHistorySize = Min(
        ActionSelectionModule_TMotorCortexHistorySize+1,
        maxMotorCortexHistorySize);
    ActionSelectionModule_TiMotorCortexHistoryLoop = Mod(
        ActionSelectionModule_TiMotorCortexHistoryLoop, 
        maxMotorCortexHistorySize)+1;
    ActionSelectionModule_TMotorCortexHistory.Set(
        ActionSelectionModule_TiMotorCortexHistoryLoop,All,All,MotorCortex);
    MotorCortexHistoryMean = light_total(
        ActionSelectionModule_TMotorCortexHistory(R(1,
        ActionSelectionModule_TMotorCortexHistorySize),All,All))/
        ActionSelectionModule_TMotorCortexHistorySize;
    MotorCortexOut = -(ElemProduct (GainMatrix, MotorCortexHistoryMean))+log(
        light_total(light_flatten(exp(ElemProduct (GainMatrix, 
        MotorCortexHistoryMean)))));
    if (LightMin(light_flatten(MotorCortexOut)) < threshold)
    {
        ActionSelectionModule_TMotorCortexHistorySize = 0;
        ActionSelectionModule_TiMotorCortexHistoryLoop = 0;
        ActionSelectionModule_TMotorCortexHistory.Set(1,All,All,0.*
            MotorCortexHistoryMean);
    }
    return MotorCortexOut;
}

int ActionSelectionModule_TiMotorCortexHistoryLoop;
int ActionSelectionModule_TMotorCortexHistorySize;
doubleNNN ActionSelectionModule_TMotorCortexHistory(5, 41, 41);
