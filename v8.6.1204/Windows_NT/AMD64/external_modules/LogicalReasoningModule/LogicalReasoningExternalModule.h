#ifndef LogicalReasoningExternalModule_h
#define LogicalReasoningExternalModule_h

#define LM_NNNN

/* Level of the library */
#include "lightmat.h"

doubleNN LogicalReasoningModule ( const int &, const 
    double &, const double &, const double &, const int &, const double &, 
    const int &, const doubleN &, const doubleN &, const doubleN &, const 
    doubleN &, const doubleN &, const doubleN &, const doubleN &, const doubleN 
    &, const doubleN &, const doubleN &, const int &, const doubleN &, const 
    intN &, const doubleN &, const intN &, const intN &, const intN &, const 
    int &, const doubleN &, const intN &, const intN &, const int &, const 
    doubleN &, const int &, const doubleN &, const intN &, const int &, const 
    doubleN &, const intN &, const doubleN &, const intN &, const doubleN &, 
    const intN &, const doubleN &, const int &, const doubleN &);

void LogicalReasoningModuleInit ();


#endif
