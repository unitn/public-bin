#include "ActionSelectionExternalModule.h"
#include <math.h>

double wLl = 1.0;		/* weight for change lane left */
double wLc = 1.0;		/* weight for stay in current lane */
double wLr = 1.0;		/* weight for change right  */
double wA = 1.0;		/* weight for advance (i.e. accelerate) */
double wD = 1.0;		/* weight for deccelerate  */
double wThrsh = 1.0;	/* weight for change MSPRT thrshold */

void ActionSelectionModuleInit ()
{
    int i;
    int j;
    int k;
    iMotorCortexHistoryLoop = 0;
    MotorCortexHistory.SetShape(Max(5,0), Max(41,0), Max(41,0) );
    k = 1;
    while (k <= 5)
    {
        j = 1;
        while (j <= 41)
        {
            i = 1;
            while (i <= 41)
            {
                MotorCortexHistory(k, j, i) = 0.;
                i = i+1;
            }
            j = j+1;
        }
        k = k+1;
    }
    MotorCortexHistorySize = 0;;
}

doubleNN BiasAndActionSelection (const doubleNN &MotorCortex, const doubleNNN &BiasMatrices, const doubleN &SemanticCodes, const doubleN &GuessWeights)
{
    doubleN Weights(0);
    doubleNN GainMatrix(0, 0);
    doubleNN InhibitionMatrix(0, 0);
    doubleNN MotorCortexOut(0, 0);
    int j;
    int i;

	/*The initial weight values*/
	/*Here is where Reinforcement Learning can be used to modify Weights - using SemanticCodes as a descriptor of the current situation*/
    Weights = GuessWeights;

	/*Set the dimensions of the GainMatrix*/
	/*Weights can be either positive or negative*/
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2),0), Max(BiasMatrices.dimension(3),0));

    i = 1;
    while (i <= BiasMatrices.dimension(2))
    {
        j = 1;
        while (j <= BiasMatrices.dimension(3))
        {
            GainMatrix(i, j) = 1.+BiasMatrices(1, i, j)*Weights(1)+LightMax(ElemProduct(BiasMatrices(R(2,-1), i, j),Weights(R(2,-1))));
            j = j+1;
        }
        i = i+1;
    }

	/*Set the dimensions of the InhibitionMatrix*/
    InhibitionMatrix.SetShape(Max(BiasMatrices.dimension(2),0), Max(BiasMatrices.dimension(3),0));

    i = 1;
    while (i <= BiasMatrices.dimension(2))
    {
        j = 1;
        while (j <= BiasMatrices.dimension(3))
        {
            InhibitionMatrix(i, j) = 1.-LightMax(ElemProduct(BiasMatrices(R(2,-1), i, j), -Weights(R(2,-1))));
            j = j+1;
        }
        i = i+1;
    }

	/*Set the dimensions of the GainMatrix*/
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2),0), Max(BiasMatrices.dimension(3),0) );

    i = 1;
    while (i <= BiasMatrices.dimension(2))
    {
        j = 1;
        while (j <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i+-1, 1+j+-1) = GainMatrix(i, j)*Max(0.,InhibitionMatrix(i, j));
            j = j+1;
        }
        i = i+1;
    }

    return MotorCortexOut = MSPRT (MotorCortex, GainMatrix);
}

int getMotorCortexHistorySize ()
{
    return MotorCortexHistorySize;
}

int getiMotorCortexHistoryLoop ()
{
  return iMotorCortexHistoryLoop;
}

doubleNNN getMotorCortexHistory ()
{
    return MotorCortexHistory;
}

doubleNN MSPRT ( const doubleNN &MotorCortex, const doubleNN &GainMatrix)
{
    int i;
    int j;
    doubleNN MotorCortexHistoryMean(41, 41);
    doubleNN MotorCortexOut(41, 41);
    double threshold;
    int maxMotorCortexHistorySize;
    static bool reset = true;

    /*If the value is less than the threshold, reset dataWindow to length=1*/
    if (reset)
    {
        reset = false;
        MotorCortexHistorySize = 0;
        iMotorCortexHistoryLoop = 0;
        MotorCortexHistory.Set(1,All,All,0.*MotorCortexHistoryMean);
    }

	/*Settings*/
    threshold = 6.9;
    maxMotorCortexHistorySize = MotorCortexHistory.dimension(1);
    MotorCortexHistorySize = Min(MotorCortexHistorySize+1, maxMotorCortexHistorySize);
    iMotorCortexHistoryLoop = Mod(iMotorCortexHistoryLoop, maxMotorCortexHistorySize)+1;

	/*Update indexes of the MotorCortex*/
    MotorCortexHistory.Set(iMotorCortexHistoryLoop,All,All,MotorCortex);

	/*Update MotorCortex window and calculate the mean*/
	MotorCortexHistoryMean = light_total(MotorCortexHistory(R(1,MotorCortexHistorySize),All,All)) / MotorCortexHistorySize;

	/*Calculate output MotorCortex*/
    MotorCortexOut = -(ElemProduct (GainMatrix, MotorCortexHistoryMean))+log(light_total(light_flatten(exp(ElemProduct (GainMatrix, MotorCortexHistoryMean)))));

	/*If the value is less than the threshold, reset dataWindow to length=1*/
    if (LightMin(light_flatten(MotorCortexOut)) < threshold)
    {
        reset = true;
    }
    return MotorCortexOut;
}

int iMotorCortexHistoryLoop;
int MotorCortexHistorySize;
doubleNNN MotorCortexHistory(5, 41, 41);
