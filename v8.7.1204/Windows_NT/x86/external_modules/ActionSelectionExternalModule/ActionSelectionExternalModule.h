#ifndef ActionSelectionExternalModule_h
#define ActionSelectionExternalModule_h

#define LM_NNNN
/* Level of the library */
#include "lightmat.h"

void ActionSelectionModuleInit ();

doubleNN BiasAndActionSelection ( const doubleNN &, const doubleNNN &, const doubleN &, const doubleN &);

int getMotorCortexHistorySize ();

doubleNN MSPRT ( const doubleNN &, const doubleNN &);

extern int iMotorCortexHistoryLoop;
extern int MotorCortexHistorySize;
extern doubleNNN MotorCortexHistory;

#endif
