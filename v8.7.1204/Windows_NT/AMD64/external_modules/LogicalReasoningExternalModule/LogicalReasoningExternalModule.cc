#include "LogicalReasoningExternalModule.h"

#include <math.h>


doubleNN LogicalReasoningModule (
	const int &ID, 
	const double &AVItime, 
	const double &ALgtFild, 
	const double &VLgtFild, 
	const int &CurrentLane, 
	const double &LaneWidth, 
	const int &NrObjs, 
	const intN &ObjID, 
	const intN &ObjClass, 
	const intN &ObjSensorInfo, 
	const intN &ObjFoundVect, 
	const doubleN &Objs0Vect, 
	const doubleN &Objn0Vect, 
	const doubleN &Objb0Vect, 
	const doubleN &ObjVel, 
	const doubleN &ObjAcc, 
	const doubleN &ObjCourse, 
	const doubleN &ObjLen, 
	const doubleN &ObjWidth, 
	const doubleN &ObjLane, 
	const int &NrStubs, 
	const doubleN &StubDist, 
	const intN &ConnectingPathId, 
	const doubleN &TurnAngle, 
	const intN &RightOfWay, 
	const intN &StubNrLanesDrivingDirection, 
	const intN &StubNrLanesOppositeDirection, 
    const int &NrLaneConnectivity, 
	const doubleN &LaneConnectivityDist, 
	const intN &SuccLaneNr, 
	const intN &SuccLanePathId, 
	const intN &FirstPredLaneNr, 
    const doubleN &FirstPredLaneSideOffset, 
	const intN &LastPredLaneNr, 
	const doubleN &LastPredLaneSideOffset, 
	const intN &PredLanesPathId, 
	const int &NrTrfLights, 
	const doubleN &TrfLightDist, 
	const intN &TrfLightCurrState, 
    const doubleN &TrfLightFirstTimeToChange, 
	const intN &TrfLightFirstNextState, 
    const doubleN &TrfLightSecondTimeToChange, 
	const intN &TrfLightSecondNextState, 
    const doubleN &TrfLightThirdTimeToChange, 
	const int &NrPedCross, 
	const doubleN &PedCrossDist)
{
    int NrBoundingBoxes;
    doubleNN BoundingBoxes(0, 0);
    int i;
    int j;

	/*maximum number of Bounding Boxes*/
    BoundingBoxes.SetShape(Max(10,0), Max(7,0) );
    i = 1;
    while (i <= 10)
    {
        j = 1;
        while (j <= 7)
        {
            BoundingBoxes(1+i+-1, 1+j+-1) = 0.;
            j = j+1;
        }
        i = i+1;
    }

	/*The LRM computation stays here*/

	/*The output of the LRM must fill the BoundingBoxes array*/
    NrBoundingBoxes = 0;
    i = 1;
    while (i <= NrBoundingBoxes)
    {
        BoundingBoxes(i, 1) = 2.;  /*1: time ahead*/
        BoundingBoxes(i, 2) = 0.5;  /*2: n left bottom corner*/
        BoundingBoxes(i, 3) = 5.;  /*3: s left bottom corner*/
        BoundingBoxes(i, 4) = 5.;  /*4: box width*/
        BoundingBoxes(i, 5) = 5.;  /*5: box height*/
        BoundingBoxes(i, 6) = 0.;  /*6: box salience 0-2 (1 is neutral) (salience - 1 = weight) */ 
        BoundingBoxes(i, 7) = .5; /*7: SemanticCode*/
        i = i+1;
    }
    return BoundingBoxes(R(1,NrBoundingBoxes),All); /*Trim BoundingBoxes Array*/
}

void LogicalReasoningModuleInit ()
{
; 
}

