#ifndef LogicalReasoningExternalModule_h
#define LogicalReasoningExternalModule_h

#define LM_NNNN

/* Level of the library */
#include "lightmat.h"

doubleNN LogicalReasoningModule (
	const int &ID, 
	const double &AVItime, 
	const double &ALgtFild, 
	const double &VLgtFild,
	const int &CurrentLane, 
	const double &LaneWidth, 
	const int &NrObjs, 
	const intN &ObjID,
	const intN &ObjClass, 
	const intN &ObjSensorInfo, 
	const intN &ObjFoundVect,
	const doubleN &Objs0Vect, 
	const doubleN &Objn0Vect, 
	const doubleN &Objb0Vect,
	const doubleN &ObjVel, 
	const doubleN &ObjAcc, 
	const doubleN &ObjCourse,
	const doubleN &ObjLen, 
	const doubleN &ObjWidth, 
	const doubleN &ObjLane,
	const int &NrStubs, 
	const doubleN &StubDist, 
	const intN &ConnectingPathId,
	const doubleN &TurnAngle, 
	const intN &RightOfWay,
	const intN &StubNrLanesDrivingDirection, 
	const intN &StubNrLanesOppositeDirection,
	const int &NrLaneConnectivity, 
	const doubleN &LaneConnectivityDist,
	const intN &SuccLaneNr, 
	const intN &SuccLanePathId, 
	const intN &FirstPredLaneNr,
	const doubleN &FirstPredLaneSideOffset, 
	const intN &LastPredLaneNr,
	const doubleN &LastPredLaneSideOffset, 
	const intN &PredLanesPathId,
	const int &NrTrfLights, 
	const doubleN &TrfLightDist, 
	const intN &TrfLightCurrState,
	const doubleN &TrfLightFirstTimeToChange, 
	const intN &TrfLightFirstNextState,
	const doubleN &TrfLightSecondTimeToChange, 
	const intN &TrfLightSecondNextState,
	const doubleN &TrfLightThirdTimeToChange, 
	const int &NrPedCross,
	const doubleN &PedCrossDist);

void LogicalReasoningModuleInit ();


#endif
