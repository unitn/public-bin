#ifndef ActionSelectionExternalModule_h
#define ActionSelectionExternalModule_h

#define LM_NNNN
/* Level of the library */
#include "lightmat.h"

extern double wLl;		/* weight for change lane left */
extern double wLc;		/* weight for stay in current lane */
extern double wLr;		/* weight for change right  */
extern double wA;		/* weight for advance (i.e. accelerate) */
extern double wD;		/* weight for deccelerate  */
extern double wThrsh;	/* weight for change MSPRT thrshold */

void ActionSelectionModuleInit ();

doubleNN BiasAndActionSelection ( const doubleNN &, const doubleNNN &, const doubleN &, const doubleN &);

int getMotorCortexHistorySize ();

doubleNN MSPRT ( const doubleNN &, const doubleNN &);

extern int iMotorCortexHistoryLoop;
extern int MotorCortexHistorySize;
extern doubleNNN MotorCortexHistory;

#endif
