/* ===============================================================================

  Client test for Server with co-driver for UDP communication

  Authors: Alessandro Mazzalai, Gastone Pietro Rosati Papini and Riccardo Don�

 =============================================================================== */

#if defined(_WIN32)
#pragma comment (lib, "Ws2_32.lib")
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <stdlib.h>
#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <direct.h>
#include <stdint.h>

#elif defined(__MACH__) || defined(__linux__)
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <codriver_interfaces_data_structs.h>

#include "codriver_public_lib.h"

#define DEFAULT_SERVER_IP  "127.0.0.1"
#define SERVER_PORT             30000  // Server port


int main(void)
{
	// Messages variables
	input_data_str scenario_msg = {}; //Inizialization to zeros
	output_data_str manoeuvre_msg = {}; //Inizialization to zeros
    uint32_t message_id = 0;

	fprintf(stdout, "Size Scenario: %lu\n", (unsigned long)sizeof(input_data_str));
	fprintf(stdout, "Size Manoeuvre: %lu\n", (unsigned long)sizeof(output_data_str));

	// Init Client
#ifdef REMOTELIB
	client_codriver_init(DEFAULT_SERVER_IP, SERVER_PORT);
#else
	client_codriver_init(NULL, SERVER_PORT);
#endif

	// This is are test numbers
	scenario_msg.ID = 1;            //TEST NUMBER
	scenario_msg.VLgtFild = 5.0;    //TEST NUMBER
	scenario_msg.RequestedCruisingSpeed = 20.0;
	scenario_msg.LaneWidth = 5.0;   //TEST NUMBER
	scenario_msg.TrfLightThirdTimeToChange = 7; //TEST NUMBER
	scenario_msg.SteerWhlAg = 0.5;  //TEST NUMBER
	scenario_msg.YawRateFild = 0.02; //TEST NUMBER

	for (int i= 0; i<50; i++){
		scenario_msg.CycleNumber = i; //TEST NUMBER
		// Send and receive operations
		client_codriver_compute(&scenario_msg, &manoeuvre_msg);
		// If you get the same numbers the server void works
		printf("ID = %d\n", manoeuvre_msg.ID);                   //CHECK TEST NUMBER
		printf("CycleNumber = %d\n", manoeuvre_msg.CycleNumber); //CHECK TEST NUMBER
		printf("Status = %d\n", manoeuvre_msg.Status);
#if CODRIVER_INTERFACES_VERSION == 1113
		printf("J0 = %f\n", manoeuvre_msg.J0f);                 //CHECK TEST NUMBER
#else
		printf("J0 = %f\n", manoeuvre_msg.J0);                 //CHECK TEST NUMBER
#endif
	}

	// Close socket
	client_codriver_close();
	printf("Done\n");
	return 0;
}
