/* ============================================================================

 UDP communication function

 Author: Gastone Pietro Rosati Papini

 ============================================================================ */

#ifndef __CODRIVER_CLIENT_LIB_H
#define __CODRIVER_CLIENT_LIB_H

#if defined(_WIN32)
#pragma comment (lib, "Ws2_32.lib")
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Winsock2.h>
#include <Ws2tcpip.h>

#elif defined(__MACH__) || defined(__linux__)
#include <arpa/inet.h>
#include <sys/socket.h>
#endif

#include "codriver_interfaces_data_structs.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DLL_EXPORT
#define USE_DLL __declspec(dllexport)
#elif DLL_IMPORT
#define USE_DLL __declspec(dllimport)
#else
#define USE_DLL
#endif

// Client codriver init for matlab
USE_DLL	void client_codriver_init_num(unsigned int *num_ip = NULL, int server_port = 0);

// Client codriver init
USE_DLL void client_codriver_init(const char *server_ip = NULL, int server_port = 0);

// Client send
USE_DLL int client_codriver_send(volatile uint32_t server_run, uint32_t message_id, input_data_str* scenario_msg);

// Client receive
USE_DLL int client_codriver_receive(uint32_t *server_run, uint32_t *message_id, output_data_str* manoeuvre_msg, uint64_t start_time);

// Client for codriver compute: send and receive in one function
USE_DLL void client_codriver_compute(input_data_str* scenario_msg, output_data_str* manoeuvre_msg);

// Client receive
USE_DLL void client_codriver_close();

// Client Get time
USE_DLL uint64_t client_get_time_ms();

#ifdef __cplusplus
}
#endif

#endif
