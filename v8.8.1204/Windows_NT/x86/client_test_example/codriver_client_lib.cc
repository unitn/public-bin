/* ============================================================================

 UDP communication for Codriver Client

 Author: Gastone Pietro Rosati Papini

 ============================================================================ */

#include "udp_functions.h"
#include "codriver_client_lib.h"
#include "codriver_interfaces_data_structs.h"


#include <stdlib.h>
#include <stdio.h>
#include <sstream>


static struct {
	int socket_id = -1;
	struct sockaddr_in server_addr;
	int packet_size;
} socket_config;


uint32_t message_id = 0;
bool initialized = false;

#ifdef __cplusplus
extern "C" {
#endif

// Client Codriver Init Matlab=================================================================
void client_codriver_init_num(unsigned int *num_ip, int server_port) {
	if (num_ip == NULL) {//LOCAL CODRIVER
							// INIT CODRIVER
							// codriver_TcodriverInit();
	}
	else {
		static bool initialized = false;
		if (initialized == true) {
			printf("The system is already initialized.\n");
			exit(EXIT_FAILURE);
		}
		// Init Client
		std::string ip_str = "";
		std::ostringstream ip_0, ip_1, ip_2, ip_3;

		ip_0 << num_ip[0];
		ip_1 << num_ip[1];
		ip_2 << num_ip[2];
		ip_3 << num_ip[3];

		ip_str = ip_0.str() + "." + ip_1.str() + "." + ip_2.str() + "." + ip_3.str();;

		memset(&socket_config.server_addr, 0, sizeof(socket_config.server_addr));
		socket_config.server_addr.sin_family = AF_INET;
		socket_config.server_addr.sin_port = htons(server_port);
		socket_config.server_addr.sin_addr.s_addr = inet_addr(ip_str.c_str());
		if ((socket_config.socket_id = open_socket(false, &socket_config.server_addr)) == -1) {
			exit(EXIT_FAILURE);
		}
	}
}

// Client Codriver Init =================================================================
void client_codriver_init(const char *server_ip, int server_port) {
	if (server_ip == NULL) {//LOCAL CODRIVER
		// INIT CODRIVER
		// codriver_TcodriverInit();
	} else {
		if (initialized == true) {
			printf("The system is already initialized.\n");
			exit(EXIT_FAILURE);
		}
		memset(&socket_config.server_addr, 0, sizeof(socket_config.server_addr));
		socket_config.server_addr.sin_family = AF_INET;
		socket_config.server_addr.sin_port = htons(server_port);
		socket_config.server_addr.sin_addr.s_addr = inet_addr(server_ip);
		if ((socket_config.socket_id = open_socket(false, &socket_config.server_addr)) == -1) {
			exit(EXIT_FAILURE);
		}
	}
}

// Client Codriver Send =================================================================
int client_codriver_send(uint32_t server_run, uint32_t message_id, input_data_str* scenario_msg) {
	return send_message(socket_config.socket_id, &socket_config.server_addr, server_run, message_id, (char*)scenario_msg, sizeof(input_data_str));
}

// Client Codriver Receive ===============================================================
int client_codriver_receive(uint32_t *server_run, uint32_t *message_id, output_data_str* manoeuvre_msg, uint64_t start_time) {
	return receive_message(socket_config.socket_id, &socket_config.server_addr, server_run, message_id, (char *)manoeuvre_msg, sizeof(output_data_str), start_time);
}

// Client Codriver Compute =================================================================
void client_codriver_compute(input_data_str* scenario_msg, output_data_str* manoeuvre_msg) {
	if (socket_config.socket_id == -1) {//LOCAL CODRIVER
		// CODRIVER COMPUTE
	} else {
		uint32_t server_run = 1;
		uint64_t start_time = get_time_ms();
		if (manoeuvre_msg == NULL)
			server_run = 0;
		if (send_message(socket_config.socket_id, &socket_config.server_addr, server_run, message_id, (char*)scenario_msg, sizeof(input_data_str)) == -1) {
			fprintf(stderr, "Warning! Couldn't send the message with ID %d\n", message_id);
		} else {
			if (receive_message(socket_config.socket_id, &socket_config.server_addr, &server_run, &message_id, (char *)manoeuvre_msg, sizeof(output_data_str), start_time) == -1) {
				fprintf(stderr, "Output message: manoeuvre not calculated\n");
			}
		}
		message_id++;
	}
}

// Client Codriver Close =================================================================
void client_codriver_close() {
	if (socket_config.socket_id == -1) {//LOCAL CODRIVER
						  // CODRIVER COMPUTE
	} else {
		char a;
		if (send_message(socket_config.socket_id, &socket_config.server_addr, 0, message_id, &a, 1) == -1) {
			fprintf(stderr, "Warning! Couldn't send the message with ID %d\n", message_id);
		}
		close_socket(socket_config.socket_id);
	}
}

uint64_t client_get_time_ms() {
	return get_time_ms();
}

#ifdef __cplusplus
}
#endif
