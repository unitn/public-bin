#ifndef LogicalReasoningExternalModule_h
#define LogicalReasoningExternalModule_h

#define LM_NNNN

extern int test;

/* Level of the library */
#include "lightmat.h"

doubleNN LogicalReasoningModule (
	const int &ID, 
	const double &AVItime, 
	const double &ALgtFild, 
	const double &VLgtFild,
	const int &CurrentLane, 
	const double &LaneWidth, 
	const int &NrObjs, 
	const intN &ObjID,
	const intN &ObjClass, 
	const intN &ObjSensorInfo, 
	const intN &ObjFoundVect,
	const doubleN &Objs0Vect, 
	const doubleN &Objn0Vect, 
	const doubleN &Objb0Vect,
	const doubleN &ObjVel, 
	const doubleN &ObjAcc, 
	const doubleN &ObjCourse,
	const doubleN &ObjLen, 
	const doubleN &ObjWidth, 
	const doubleN &ObjLane,
	const int &NrStubs, 
	const doubleN &StubDist, 
	const intN &ConnectingPathId,
	const doubleN &TurnAngle, 
	const intN &RightOfWay,
	const intN &StubNrLanesDrivingDirection, 
	const intN &StubNrLanesOppositeDirection,
	const int &NrLaneConnectivity, 
	const doubleN &LaneConnectivityDist,
	const intN &SuccLaneNr, 
	const intN &SuccLanePathId, 
	const intN &FirstPredLaneNr,
	const doubleN &FirstPredLaneSideOffset, 
	const intN &LastPredLaneNr,
	const doubleN &LastPredLaneSideOffset, 
	const intN &PredLanesPathId,
	const int &NrTrfLights, 
	const doubleN &TrfLightDist, 
	const intN &TrfLightCurrState,
	const doubleN &TrfLightFirstTimeToChange, 
	const intN &TrfLightFirstNextState,
	const doubleN &TrfLightSecondTimeToChange, 
	const intN &TrfLightSecondNextState,
	const doubleN &TrfLightThirdTimeToChange, 
	const int &NrPedCross,
	const doubleN &PedCrossDist);

void LogicalReasoningModuleInit ();

void ASM_function(double VLgtFild, double CurrentLane, double Objs0Vect_aux[], double Objn0Vect_aux[], double ObjVel_aux[], double ObjLen_aux[], double ObjWidth_aux[], double ObjLane_aux[], double Boxes[],int  N_of_B,double Weights[]);
void LRM_Function_14output(double Time_A, double num1, double num2, double vector1[], double vector2[], double vector3[], double vector4[], double vector5[], double vector6[], double output[], int& N_of_B);
void Make_Boxes(double Time_A, double num1_MB, double num2_MB, double vector1_MB[], double vector2_MB[], double vector3_MB[], double vector4_MB[], double vector5_MB[], double vector6_MB[], double output_MB[]);
void Combine_Close_Boxes(double Input_vector[], double Output_vector[]);
void Add_Gaps(double num2, double Input_vector[], double Output_vector[]);
void Add_Absolute_Box_Lane(double Input_vector[], double Output_vector[]);
void Add_More(double Input_vector[], double Output_vector[]);
void Add_Relative_Location(double Input_vector[], double Output_vector[]);
void Add_Config(double Input_vector[], double Output_vector[]);
void Add_Adjecancy(double Input_vector[], double Output_vector[]);
void Add_Whole_Config(double Input_vector[], double Output_vector[]);
void Add_Annotations(double Input_vector[], double Output_vector[], int& N_R);
void Add_Num_of_Rows(int N_R, double Input_vector[], double Output_vector[]);
void Mysort(double Input_vector[], double Output_vector[]);
double Mymax(double num1, double num2);
void Add_Gaps_One_Lane(double num2, double Input_vector[], double Output_vector[]);
double Mybi2dec(double Input_vector[]);


#endif
