#include "LogicalReasoningExternalModule.h"
#include <math.h>
#include <memory>
//
//int test = 1.0;
//
//doubleNN LogicalReasoningModule_NEW(
//	const int &ID,
//	const double &AVItime,
//	const double &ALgtFild,
//	const double &VLgtFild,
//	const int &CurrentLane,
//	const double &LaneWidth,
//	const int &NrObjs,
//	const intN &ObjID,
//	const intN &ObjClass,
//	const intN &ObjSensorInfo,
//	const intN &ObjFoundVect,
//	const doubleN &Objs0Vect,
//	const doubleN &Objn0Vect,
//	const doubleN &Objb0Vect,
//	const doubleN &ObjVel,
//	const doubleN &ObjAcc,
//	const doubleN &ObjCourse,
//	const doubleN &ObjLen,
//	const doubleN &ObjWidth,
//	const doubleN &ObjLane,
//	const int &NrStubs,
//	const doubleN &StubDist,
//	const intN &ConnectingPathId,
//	const doubleN &TurnAngle,
//	const intN &RightOfWay,
//	const intN &StubNrLanesDrivingDirection,
//	const intN &StubNrLanesOppositeDirection,
//    const int &NrLaneConnectivity,
//	const doubleN &LaneConnectivityDist,
//	const intN &SuccLaneNr,
//	const intN &SuccLanePathId,
//	const intN &FirstPredLaneNr,
//    const doubleN &FirstPredLaneSideOffset,
//	const intN &LastPredLaneNr,
//	const doubleN &LastPredLaneSideOffset,
//	const intN &PredLanesPathId,
//	const int &NrTrfLights,
//	const doubleN &TrfLightDist,
//	const intN &TrfLightCurrState,
//    const doubleN &TrfLightFirstTimeToChange,
//	const intN &TrfLightFirstNextState,
//    const doubleN &TrfLightSecondTimeToChange,
//	const intN &TrfLightSecondNextState,
//    const doubleN &TrfLightThirdTimeToChange,
//	const int &NrPedCross,
//	const doubleN &PedCrossDist)
//{
//    int NrBoundingBoxes;
//    doubleNN BoundingBoxes(0, 0);
//    int i;
//    int j;
//
//	cout << "GASTONE :"<< test <<'\n';
//
//	/*maximum number of Bounding Boxes*/
//    BoundingBoxes.SetShape(Max(10,0), Max(7,0) );
//    i = 1;
//    while (i <= 10)
//    {
//        j = 1;
//        while (j <= 7)
//        {
//            BoundingBoxes(1+i+-1, 1+j+-1) = 0.;
//            j = j+1;
//        }
//        i = i+1;
//    }
//
//	/*The LRM computation stays here*/
//
//    FILE* outputforMatlab;
//    FILE* boxesfromMatlab;
//
//    outputforMatlab = fopen("OneLineIn","w");
//
//    fprintf(outputforMatlab,"%lf %lf %lf %d %lf",AVItime,ALgtFild,VLgtFild,CurrentLane,LaneWidth);
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%i ",ObjFoundVect(j+1));
//        }
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",Objs0Vect(j+1));
//        }
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",Objn0Vect(j+1));
//        }
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",Objb0Vect(j+1));
//        //    printf("%lf ",ObjB(j));
//        }
//        //printf("\n");
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",ObjVel(j+1));
//        //    printf("%lf ",ObjV(j));
//        }
//        //printf("\n");
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",ObjAcc(j+1));
//        //    printf("%lf ",ObjA(j));
//        }
//        //printf("\n");
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",ObjCourse(j+1));
//        //    printf("%lf ",ObjCourse(j));
//        }
//        //printf("\n");
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",ObjLen(j+1));
//        //    printf("%lf ",ObjLen(j));
//        }
//        //printf("\n");
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",ObjWidth(j+1));
//        //    printf("%lf ",ObjWidth(j));
//        }
//        //printf("\n");
//        for(j=0;j<NrObjs;j++){
//            fprintf(outputforMatlab,"%lf ",ObjLane(j+1));
//        //    printf("%lf ",ObjLane(j));
//        }
//
//        fclose(outputforMatlab);
//
//        //system("matlab -nosplash -r \"run MatlabFromC.m;\"");
//
//        boxesfromMatlab = fopen("OneLineOut","r");
//
//        char s[10000];
//        fgets(s,10000,boxesfromMatlab);
//
//        fclose(boxesfromMatlab);
//
//        i=0;
//
//        //for (i=0; s[i]; s[i]==',' ? i++ : *s++); // i counts number of commas in csv line
//
//        for(j=0;j<10000;j++){
//            if(s[j]==',')
//                i++; // i counts number of commas in csv line
//        }
//
//        i = (i-1)/7;
//
//
//    doubleNN pp(7,i);
//
//    boxesfromMatlab = fopen("OneLineOut","r");
//
//    double dummy;
//    double ppp;
//    int k;
//
//    fscanf(boxesfromMatlab,"%lf,",&dummy);
//
//    for(j=0;j<i;j++){
//        for(k=0;k<7;k++){
//            fscanf(boxesfromMatlab,"%lf,",&ppp);
//            pp(k+1,j+1) = ppp;
//        //    printf("%lf ",pp[j][k]);
//        }
//        //printf("\n");
//    }
//    fclose(boxesfromMatlab);
//
//
//	/*The output of the LRM must fill the BoundingBoxes array*/
//    NrBoundingBoxes = i;
//    j = 1;
//    while (j <= NrBoundingBoxes)
//    {
//        BoundingBoxes(j, 1) = pp(1,j);  /*1: time ahead*/
//        BoundingBoxes(j, 2) = pp(2,j);  /*2: n left bottom corner*/
//        BoundingBoxes(j, 3) = pp(3,j);  /*3: s left bottom corner*/
//        BoundingBoxes(j, 4) = pp(4,j);  /*4: box width*/
//        BoundingBoxes(j, 5) = pp(5,j);  /*5: box height*/
//        BoundingBoxes(j, 6) = pp(6,j);  /*6: box salience 0-2 (1 is neutral) (salience - 1 = weight) */
//        BoundingBoxes(j, 7) = pp(7,j); /*7: SemanticCode*/
//        j = i+1;
//    }
//    return BoundingBoxes(R(1,NrBoundingBoxes),All); /*Trim BoundingBoxes Array*/
//}

#ifdef EXTERNAL_MODULES_LRM

doubleNN LogicalReasoningModule(
	const int &ID,
	const double &AVItime,
	const double &ALgtFild,
	const double &VLgtFild,
	const int &CurrentLane,
	const double &LaneWidth,
	const int &NrObjs,
	const intN &ObjID,
	const intN &ObjClass,
	const intN &ObjSensorInfo,
	const intN &ObjFoundVect,
	const doubleN &Objs0Vect,
	const doubleN &Objn0Vect,
	const doubleN &Objb0Vect,
	const doubleN &ObjVel,
	const doubleN &ObjAcc,
	const doubleN &ObjCourse,
	const doubleN &ObjLen,
	const doubleN &ObjWidth,
	const doubleN &ObjLane,
	const int &NrStubs,
	const doubleN &StubDist,
	const intN &ConnectingPathId,
	const doubleN &TurnAngle,
	const intN &RightOfWay,
	const intN &StubNrLanesDrivingDirection,
	const intN &StubNrLanesOppositeDirection,
	const int &NrLaneConnectivity,
	const doubleN &LaneConnectivityDist,
	const intN &SuccLaneNr,
	const intN &SuccLanePathId,
	const intN &FirstPredLaneNr,
	const doubleN &FirstPredLaneSideOffset,
	const intN &LastPredLaneNr,
	const doubleN &LastPredLaneSideOffset,
	const intN &PredLanesPathId,
	const int &NrTrfLights,
	const doubleN &TrfLightDist,
	const intN &TrfLightCurrState,
	const doubleN &TrfLightFirstTimeToChange,
	const intN &TrfLightFirstNextState,
	const doubleN &TrfLightSecondTimeToChange,
	const intN &TrfLightSecondNextState,
	const doubleN &TrfLightThirdTimeToChange,
	const int &NrPedCross,
	const doubleN &PedCrossDist)
{
	int NrBoundingBoxes;
	doubleNN BoundingBoxes(0, 0);
	int i;
	int j;

	/*maximum number of Bounding Boxes*/
	BoundingBoxes.SetShape(Max(20, 0), Max(15, 0));
	i = 1;
	while (i <= 20)
	{
		j = 1;
		while (j <= 15)
		{
			BoundingBoxes(1 + i + -1, 1 + j + -1) = 0.;
			j = j + 1;
		}
		i = i + 1;
	}


	int numObjs0Vect = Objs0Vect.dimension(1);
	int numObjn0Vect = Objn0Vect.dimension(1);
	int numObjVel = ObjVel.dimension(1);
	int numObjLen = ObjLen.dimension(1);
	int numObjWidth = ObjWidth.dimension(1);
	int numObjLane = ObjLane.dimension(1);

	assert(numObjs0Vect==20);
	assert(numObjn0Vect==20);
	assert(numObjVel==20);
	assert(numObjWidth==20);
	assert(numObjLen==20);
	assert(numObjLane==20);

	double* Objs0Vect_aux = new double[numObjs0Vect];
	double* Objn0Vect_aux = new double[numObjn0Vect];
	double* ObjVel_aux = new double[numObjVel];
	double* ObjLen_aux = new double[numObjLen];
	double* ObjWidth_aux = new double[numObjWidth];
	double* ObjLane_aux = new double[numObjLane];
	int N_of_B;

	Objs0Vect.Get(Objs0Vect_aux);
	Objn0Vect.Get(Objn0Vect_aux);
	ObjVel.Get(ObjVel_aux);
	ObjLen.Get(ObjLen_aux);
	ObjWidth.Get(ObjWidth_aux);
	ObjLane.Get(ObjLane_aux);

	double Boxes[280];
	LRM_Function_14output(1, VLgtFild, CurrentLane, Objs0Vect_aux, Objn0Vect_aux, ObjVel_aux, ObjLen_aux, ObjWidth_aux, ObjLane_aux, Boxes, N_of_B);

	delete[] Objs0Vect_aux;
	delete[] Objn0Vect_aux;
	delete[] ObjVel_aux;
	delete[] ObjLen_aux;
	delete[] ObjWidth_aux;
	delete[] ObjLane_aux;

	double Weights[20];
	ASM_function(VLgtFild, CurrentLane, Objs0Vect_aux, Objn0Vect_aux, ObjVel_aux, ObjLen_aux, ObjWidth_aux, ObjLane_aux, Boxes, N_of_B, Weights);

	int numEl = 15;

	/*The output of the LRM must fill the BoundingBoxes array*/
	NrBoundingBoxes = 10; /*TODO ????????????? Please fill this number with the number of BB*/
	i = 0;
	while (i < NrBoundingBoxes)
	{
		BoundingBoxes(i+1, 1) = Boxes[i*numEl+0];   /*1: s left bottom corner*/
		BoundingBoxes(i+1, 2) = Boxes[i*numEl+1];    /*2: n left bottom corner*/
		BoundingBoxes(i+1, 3) = Boxes[i*numEl+2];    /*3: Length of the Box*/
		BoundingBoxes(i+1, 4) = Boxes[i*numEl+3];    /*4: Width of the Box*/
		BoundingBoxes(i+1, 5) = Boxes[i*numEl+4];    /*5: box salience -1:Car +1:Gap */
		BoundingBoxes(i+1, 6) = Boxes[i*numEl+5];    /*6: Lane of Ego Car, 0 = Unknown,  1 = Emergency lane, 2 = Single-lane road, 3 = Left-most lane, */
                                                     /*                    4 = Right-most lane,     5 = One of middle lanes on road with three or more lanes*/
		BoundingBoxes(i+1, 7) = Boxes[i*numEl+6];    /*7: Relative Lane of the Box: 0: the same line as Ego, -1: Inner, +1: Outer.*/
		BoundingBoxes(i+1, 8) = Boxes[i*numEl+7];    /*8: Absolute Lane of the Box (the same format as �Lane of Ego Car�).*/
		BoundingBoxes(i+1, 9) = Boxes[i*numEl+8];    /*9: Relative Location of the Box. -1: Behind the Ego Car 0: Including Ego Car +1: Adjacent Ego Car (left or right)  +2: Adjacent Ego Car (front)  +3: Front of Ego Car (but not adjacently)*/
		BoundingBoxes(i+1, 10) = Boxes[i*numEl+9];  /*Configuration context of the Box .   2468                */
		                                            /*                                     1357                */
		BoundingBoxes(i+1, 11) = Boxes[i*numEl+10];  /*11:Adjacency Vector of HABB Configuration. (binary ? decimal conversion)  e.x. [1 1 1 0 0 0] = 7*/
		BoundingBoxes(i+1, 12) = Boxes[i*numEl+11];  /*12:Whole Configuration*/
		BoundingBoxes(i+1, 13) = Boxes[i*numEl+12];  /*13:Legal Intentions (Level 0, Level 1)*/
		BoundingBoxes(i+1, 14) = Boxes[i*numEl+13];  /*14:reserved*/

		BoundingBoxes(i+1, 15) = Weights[i];
		i = i + 1;
	}
	return BoundingBoxes(R(1, NrBoundingBoxes), All); /*Trim BoundingBoxes Array*/
}

void ASM_function(double VLgtFild, double CurrentLane, double Objs0Vect_aux[], double Objn0Vect_aux[], double ObjVel_aux[], double ObjLen_aux[], double ObjWidth_aux[], double ObjLane_aux[], double Boxes[],int  N_of_B,double Weights[]){

    double intention[N_of_B];
    double lengths[N_of_B];
    int maximum = 0;
    int locOfMax = 0;
    int i;


    for (i = 0; i<N_of_B; i++)
    {
        intention[i] = Boxes[i*14+12];
        lengths[i] = Boxes[i*14*2];

        if (lengths[i] >maximum)
        {
            maximum = lengths[i];
            locOfMax = i;
        }
    }

    for (i=0; i<N_of_B; i++)
    {
        if (intention[i] > 0)
            Weights[i] = 0.5;
        else
            Weights[i] = 0;
    }
    Weights[locOfMax] = 1;

}

void LRM_Function_14output(double Time_A, double num1, double num2, double vector1[], double vector2[], double vector3[], double vector4[], double vector5[], double vector6[], double output[], int& N_of_B) {
	double Bounding_Boxes[140] = { };
	double Combined_Bounding_Boxes[140] = { };
	double Gap_Added_Boxes[140] = { };
	double Added_Absolute_Box_Lane[160] = { };
	double Added_Relative_Location[180] = { };
	double Added_Config[200] = { };
	double Added_Adjecancy[220] = { };
	double Added_Whole_Config[240] = { };
	double Added_Annotations[260] = { };
	double Added_Num_of_Rows[280] = { };

	Make_Boxes(Time_A, num1, num2, vector1, vector2, vector3, vector4, vector5, vector6, Bounding_Boxes);
	Combine_Close_Boxes(Bounding_Boxes, Combined_Bounding_Boxes);
	Add_Gaps(num2, Combined_Bounding_Boxes, Gap_Added_Boxes);
	Add_Absolute_Box_Lane(Gap_Added_Boxes, Added_Absolute_Box_Lane);
	Add_Relative_Location(Added_Absolute_Box_Lane, Added_Relative_Location);
	Add_Config(Added_Relative_Location, Added_Config);
	Add_Adjecancy(Added_Config, Added_Adjecancy);
	Add_Whole_Config(Added_Adjecancy, Added_Whole_Config);
	Add_Annotations(Added_Whole_Config, Added_Annotations, N_of_B);
	Add_Num_of_Rows(N_of_B, Added_Annotations, output);
	//N_of_B[0] = Num_of_Rows[0];
	//Add_More(Added_Num_of_Rows, output);
}

void Make_Boxes(double Time_A, double num1_MB, double num2_MB, double vector1_MB[], double vector2_MB[], double vector3_MB[], double vector4_MB[], double vector5_MB[], double vector6_MB[], double output_MB[]) {
	double output_M[140] = { };
	double out[140] = { };
	double Time_Ahead = Time_A;
	double Big_Num = 1000.0;
	for (int j=0;j<20;j=j+1){
		//boundind boxes with distance more than 20.0 meters before Ego Car, are not considered.
		if ((vector1_MB[j] != 0.0) && (vector2_MB[j] != 0.0) && (vector4_MB[j] != 0.0) && (vector5_MB[j] != 0.0) && (vector6_MB[j] != 32767) && (vector1_MB[j] - num1_MB * Time_Ahead + num1_MB * Time_Ahead + vector4_MB[j] + vector3_MB[j] * Time_Ahead > -20.0) ) {
			output_M[j * 7 + 0] = vector1_MB[j] - num1_MB * Time_Ahead;
			output_M[j * 7 + 1] = vector2_MB[j];
			output_M[j * 7 + 2] = num1_MB * Time_Ahead + vector4_MB[j] + vector3_MB[j] * Time_Ahead;
			output_M[j * 7 + 3] = vector5_MB[j];
			output_M[j * 7 + 4] = -1.0;
			output_M[j * 7 + 5] = num2_MB;
			output_M[j * 7 + 6] = vector6_MB[j];
		}
		else
		{
			output_M[j * 7 + 0] = Big_Num;
			output_M[j * 7 + 1] = Big_Num;
			output_M[j * 7 + 2] = Big_Num;
			output_M[j * 7 + 3] = Big_Num;
			output_M[j * 7 + 4] = Big_Num;
			output_M[j * 7 + 5] = num2_MB;
			output_M[j * 7 + 6] = Big_Num;
		}//if

	}//j
	Mysort(output_M,out);
	for (int j = 0; j < 140; j++) {
		output_MB[j] = out[j];
	}//j
}

void Combine_Close_Boxes(double Input_vector[], double Output_vector[]) {
	double Big_Num = 1000.0;
	double Car_Length = 4.8;
	double out[140] = {};
	double sorted[140] = {};
	for (int j = 0; j < 20 - 1; j++) {
		for (int k = j + 1; k < 20; k++) {
			if ((Input_vector[7 * j + 0] != Big_Num) && (Input_vector[7 * j + 1] != Big_Num) &&
				(Input_vector[7 * j + 4] == -1) && (Input_vector[7 * j + 6] == Input_vector[7 * k + 6]) &&
				(Input_vector[7 * k + 0] < Input_vector[7 * j + 0] + Input_vector[7 * j + 2] + Car_Length)) {
				Input_vector[7 * j + 2] = Mymax(Input_vector[7 * j + 0] + Input_vector[7 * j + 2],
												Input_vector[7 * k + 0] + Input_vector[7 * k + 2]) -
										  Input_vector[7 * j + 0];
				for (int m = 0; m < 7; m++) {
					Input_vector[7 * k + m] = Big_Num;
				}//m
				Mysort(Input_vector, sorted);
				for (int n = 0; n < 140; n++) {
					Input_vector[n] = sorted[n];
				}
			}//k
		}//if
	}//j
	//Mysort(Input_vector, out);
	for (int j = 0; j < 140; j++) {
		Output_vector[j] = Input_vector[j];
	}
}

void Add_Gaps(double num2, double Input_vector[], double Output_vector[]) {
	double out1[140] = { };
	double out2[140] = { };

	Add_Gaps_One_Lane(0.0, Input_vector, out1);

	if (num2 == 4) {
		Add_Gaps_One_Lane(1.0, out1, out2);
	} else if (num2 == 3) {
		Add_Gaps_One_Lane(-1.0, out1, out2);
	}

	for (int j = 0; j < 140; j++) {
		Output_vector[j] = out2[j];
	}

}

void Add_Absolute_Box_Lane(double Input_vector[], double Output_vector[]) {
	double Big_Num = 1000.0;
	double out[160] = {};

	for (int j = 0; j < 20; j++) {
		if ((Input_vector[7 * j + 0] != Big_Num) && (Input_vector[7 * j + 1] != Big_Num) &&
			(((Input_vector[7 * j + 5] == 4.0) && (Input_vector[7 * j + 6] != 1.0)) ||
			 ((Input_vector[7 * j + 5] == 3.0) && (Input_vector[7 * j + 6] == -1.0)))) {
			out[8 * j + 0] = Input_vector[7 * j + 0];
			out[8 * j + 1] = Input_vector[7 * j + 1];
			out[8 * j + 2] = Input_vector[7 * j + 2];
			out[8 * j + 3] = Input_vector[7 * j + 3];
			out[8 * j + 4] = Input_vector[7 * j + 4];
			out[8 * j + 5] = Input_vector[7 * j + 5];
			out[8 * j + 6] = Input_vector[7 * j + 6];
			out[8 * j + 7] = 4.0;
		}//if
		else if ((Input_vector[7 * j + 0] != Big_Num) && (Input_vector[7 * j + 1] != Big_Num) &&
				 (((Input_vector[7 * j + 5] == 3.0) && (Input_vector[7 * j + 6] != -1.0)) ||
				  ((Input_vector[7 * j + 5] == 4.0) && (Input_vector[7 * j + 6] == 1.0)))) {
			//An_Added_8(j, :) = [Boxes(j, 1:7), 3];
			out[8 * j + 0] = Input_vector[7 * j + 0];
			out[8 * j + 1] = Input_vector[7 * j + 1];
			out[8 * j + 2] = Input_vector[7 * j + 2];
			out[8 * j + 3] = Input_vector[7 * j + 3];
			out[8 * j + 4] = Input_vector[7 * j + 4];
			out[8 * j + 5] = Input_vector[7 * j + 5];
			out[8 * j + 6] = Input_vector[7 * j + 6];
			out[8 * j + 7] = 3.0;
		}//else if
		else {
			//An_Added_8(j, :) = [Boxes(j, 1:7), Big_Num];
			out[8 * j + 0] = Input_vector[7 * j + 0];
			out[8 * j + 1] = Input_vector[7 * j + 1];
			out[8 * j + 2] = Input_vector[7 * j + 2];
			out[8 * j + 3] = Input_vector[7 * j + 3];
			out[8 * j + 4] = Input_vector[7 * j + 4];
			out[8 * j + 5] = Input_vector[7 * j + 5];
			out[8 * j + 6] = Input_vector[7 * j + 6];
			out[8 * j + 7] = Big_Num;
		}//else

	}//j

	for (int j = 0; j < 160; j++) {
		Output_vector[j] = out[j];
	}

}

//This function adds more columns to the output to make it 14 Col.
void Add_More(double Input_vector[], double Output_vector[]) {
	double Big_Num = 1000.0;
	double out[280] = { };

	for (int j = 0; j < 20; j++){
		out[14 * j + 0] =  Input_vector[13 * j + 0];
		out[14 * j + 1] =  Input_vector[13 * j + 1];
		out[14 * j + 2] =  Input_vector[13 * j + 2];
		out[14 * j + 3] =  Input_vector[13 * j + 3];
		out[14 * j + 4] =  Input_vector[13 * j + 4];
		out[14 * j + 5] =  Input_vector[13 * j + 5];
		out[14 * j + 6] =  Input_vector[13 * j + 6];
		out[14 * j + 7] =  Input_vector[13 * j + 7];
		out[14 * j + 8] =  Input_vector[13 * j + 8];
		out[14 * j + 9] =  Input_vector[13 * j + 9];
		out[14 * j + 10] = Input_vector[13 * j + 10];
		out[14 * j + 11] = Input_vector[13 * j + 11];
		out[14 * j + 12] = Input_vector[13 * j + 12];
		out[14 * j + 13] = 0.0;
//			out[24 * j + 14] = 0.0;
//			out[24 * j + 15] = 0.0;
//			out[24 * j + 16] = 0.0;
//			out[24 * j + 17] = 0.0;
//			out[24 * j + 18] = 0.0;
//			out[24 * j + 19] = 0.0;
//			out[24 * j + 20] = 0.0;
//			out[24 * j + 21] = 0.0;
//			out[24 * j + 22] = 0.0;
//			out[24 * j + 23] = 0.0;
	}//j

	for (int j = 0; j < 280; j++) {
		Output_vector[j] = out[j];
	}

}

//Input columns(8): [Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane
//	 Output columns(9): [Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location]
void Add_Relative_Location(double Input_vector[], double Output_vector[]) {
	int In_index = 0;
	int Out_index = 0;
	double Big_Num = 1000.0;
	double Car_Length = 4.8;
	double out[180] = { };

	for (int j = 0; j < 20; j++){
		if ( (Input_vector[8 * j + 0] != Big_Num) && (Input_vector[8 * j + 1] != Big_Num) && (Input_vector[8 * j + 0] + Input_vector[8 * j + 2] < 0.0) ) {
			//An_Added_9(j, :) = [Boxes(j, 1:8), -1];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = -1.0;
		}
		else if ( (Input_vector[8 * j + 0] != Big_Num) && (Input_vector[8 * j + 1] != Big_Num) && (Input_vector[8 * j + 0] < Car_Length) && (Input_vector[8 * j + 0] + Input_vector[8 * j + 2] > 0.0) && (Input_vector[8 * j + 6] == 0.0) ) {
			//An_Added_9(j, :) = [Boxes(j, 1:8), 0];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 0.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] < Car_Length) && (Input_vector[8*j+0] + Input_vector[8*j+2] > 0.0) && (Input_vector[8*j+6] != 0.0)) {
			//An_Added_9(j, :) = [Boxes(j, 1:8), 1];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 1.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 4) && (In_index == 0)) {
			In_index = In_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 2];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 2.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 3.0) && (Out_index == 0)){
			Out_index = Out_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 2];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 2.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 4.0) && (In_index == 1)){
			In_index = In_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 3];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 3.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 3.0) && (Out_index == 1)) {
			Out_index = Out_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 3];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 3.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 4.0) && (In_index == 2)) {
			In_index = In_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 4];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 4.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 3.0) && (Out_index == 2)) {
			Out_index = Out_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 4];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 4.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 4.0) && (In_index == 3)) {
			In_index = In_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 5];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = 5.0;
		}
		else if ((Input_vector[8*j+0] != Big_Num) && (Input_vector[8*j+1] != Big_Num) && (Input_vector[8*j+0] > Car_Length) && (Input_vector[8*j+7] == 3.0) && (Out_index == 3)) {
			Out_index = Out_index + 1;
			//An_Added_9(j, :) = [Boxes(j, 1:8), 5];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = .0;
		}
		else {
			//An_Added_9(j, :) = [Boxes(j, 1:8), Big_Num];
			out[9 * j + 0] = Input_vector[8 * j + 0];
			out[9 * j + 1] = Input_vector[8 * j + 1];
			out[9 * j + 2] = Input_vector[8 * j + 2];
			out[9 * j + 3] = Input_vector[8 * j + 3];
			out[9 * j + 4] = Input_vector[8 * j + 4];
			out[9 * j + 5] = Input_vector[8 * j + 5];
			out[9 * j + 6] = Input_vector[8 * j + 6];
			out[9 * j + 7] = Input_vector[8 * j + 7];
			out[9 * j + 8] = Big_Num;
		}

	}//j

	for (int j = 0; j < 180; j++) {
		Output_vector[j] = out[j];
	}

}

//Input columns(9): [Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location]
//output columns(10): [Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location, Box_Config]
void Add_Config(double Input_vector[], double Output_vector[]) {
	int In3 = 0;
	int In4 = 0;
	int In5 = 0;
	int In6 = 0;
	int In7 = 0;
	int In8 = 0;
	int Box_Counter = 0;
	double Big_Num = 1000.0;
	double out[200] = { };

	for (int j = 0; j < 20; j++){
		if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == -1) && (Input_vector[9*j+7] == 4)) {
			//An_Added_10(j, :) = [Boxes(j, 1:9), 1];
			out[10 * j + 0] = Input_vector[9 * j + 0];
			out[10 * j + 1] = Input_vector[9 * j + 1];
			out[10 * j + 2] = Input_vector[9 * j + 2];
			out[10 * j + 3] = Input_vector[9 * j + 3];
			out[10 * j + 4] = Input_vector[9 * j + 4];
			out[10 * j + 5] = Input_vector[9 * j + 5];
			out[10 * j + 6] = Input_vector[9 * j + 6];
			out[10 * j + 7] = Input_vector[9 * j + 7];
			out[10 * j + 8] = Input_vector[9 * j + 8];
			out[10 * j + 9] = 1.0;
			Box_Counter = Box_Counter + 1;
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == -1) && (Input_vector[9*j+7] == 3)) {
			//An_Added_10(j, :) = [Boxes(j, 1:9), 2];
			out[10 * j + 0] = Input_vector[9 * j + 0];
			out[10 * j + 1] = Input_vector[9 * j + 1];
			out[10 * j + 2] = Input_vector[9 * j + 2];
			out[10 * j + 3] = Input_vector[9 * j + 3];
			out[10 * j + 4] = Input_vector[9 * j + 4];
			out[10 * j + 5] = Input_vector[9 * j + 5];
			out[10 * j + 6] = Input_vector[9 * j + 6];
			out[10 * j + 7] = Input_vector[9 * j + 7];
			out[10 * j + 8] = Input_vector[9 * j + 8];
			out[10 * j + 9] = 2.0;
			Box_Counter = Box_Counter + 1;
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 0) && (Input_vector[9*j+7] == 4)) {
			if (In3 == 0) {
				In3 = In3 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 3];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 3.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In3 == 1) {
				In5 = In5 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 5];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 5.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In3 == 2) {
				In7 = In7 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 7];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 7.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 0) && (Input_vector[9*j+7] == 3)) {
			if (In4 == 0) {
				In4 = In4 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 4];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 4.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In4 == 1) {
				In6 = In6 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 6];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 6.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In4 == 2) {
				In8 = In8 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 8];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 8.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 1) && (Input_vector[9*j+7] == 3)) {
			if (In4 == 0) {
				In4 = In4 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 4];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 4.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In4 == 1) {
				In6 = In6 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 6];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 6.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In4 == 2) {
				In8 = In8 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 8];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 8.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 1) && (Input_vector[9*j+7] == 4)) {
			if (In3 == 0) {
				In3 = In3 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 3];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 3.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In3 == 1) {
				In5 = In5 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 5];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 5.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In3 == 2) {
				In7 = In7 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 7];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 7.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 2) && (Input_vector[9*j+7] == 4)) {
			if (In5 == 0) {
				In5 = In5 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 5];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 5.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In5 == 1) {
				In7 = In7 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 7];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 7.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 2) && (Input_vector[9*j+7] == 3)) {
			if (In6 == 0) {
				In6 = In6 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 6];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 6.0;
				Box_Counter = Box_Counter + 1;
			}
			else if (In6 == 1) {
				In8 = In8 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 8];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 8.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 3) && (Input_vector[9*j+7] == 4)) {
			if (In7 == 0) {
				In7 = In7 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 7];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 7.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else if ((Input_vector[9*j+0] != Big_Num) && (Input_vector[9*j+1] != Big_Num) && (Input_vector[9*j+8] == 3) && (Input_vector[9*j+7] == 3)) {
			if (In8 == 0) {
				In8 = In8 + 1;
				//An_Added_10(j, :) = [Boxes(j, 1:9), 8];
				out[10 * j + 0] = Input_vector[9 * j + 0];
				out[10 * j + 1] = Input_vector[9 * j + 1];
				out[10 * j + 2] = Input_vector[9 * j + 2];
				out[10 * j + 3] = Input_vector[9 * j + 3];
				out[10 * j + 4] = Input_vector[9 * j + 4];
				out[10 * j + 5] = Input_vector[9 * j + 5];
				out[10 * j + 6] = Input_vector[9 * j + 6];
				out[10 * j + 7] = Input_vector[9 * j + 7];
				out[10 * j + 8] = Input_vector[9 * j + 8];
				out[10 * j + 9] = 8.0;
				Box_Counter = Box_Counter + 1;
			}
		}
		else {
			//An_Added_10(j, :) = [Boxes(j, 1:9), Big_Num];
			out[10 * j + 0] = Input_vector[9 * j + 0];
			out[10 * j + 1] = Input_vector[9 * j + 1];
			out[10 * j + 2] = Input_vector[9 * j + 2];
			out[10 * j + 3] = Input_vector[9 * j + 3];
			out[10 * j + 4] = Input_vector[9 * j + 4];
			out[10 * j + 5] = Input_vector[9 * j + 5];
			out[10 * j + 6] = Input_vector[9 * j + 6];
			out[10 * j + 7] = Input_vector[9 * j + 7];
			out[10 * j + 8] = Input_vector[9 * j + 8];
			out[10 * j + 9] = Big_Num;
			Box_Counter = Box_Counter + 1;
		}

	}//j


	for (int j = Box_Counter; j < 20; j++) {
		out[10 * j + 0] = Input_vector[9 * j + 0];
		out[10 * j + 1] = Input_vector[9 * j + 1];
		out[10 * j + 2] = Input_vector[9 * j + 2];
		out[10 * j + 3] = Input_vector[9 * j + 3];
		out[10 * j + 4] = Input_vector[9 * j + 4];
		out[10 * j + 5] = Input_vector[9 * j + 5];
		out[10 * j + 6] = Input_vector[9 * j + 6];
		out[10 * j + 7] = Input_vector[9 * j + 7];
		out[10 * j + 8] = Input_vector[9 * j + 8];
		out[10 * j + 9] = Big_Num;
	}

	for (int j = 0; j < 200; j++) {
		Output_vector[j] = out[j];
	}

}//function

//Input columns (10):[Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location, Box_Config]
//Output columns(11): [Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location, Box_Config, Adj_Vector]
void Add_Adjecancy(double Input_vector[], double Output_vector[]) {

	double Big_Num = 1000.0;
	double out[220] = { };

	for (int i = 0; i < 20; i++) {
		double Adj_Vector[20] = { };
		if ((Input_vector[10*i+0] != Big_Num) && (Input_vector[10*i+1] != Big_Num)) {
			for (int j = 0; j < 20; j++) {
				if ((Input_vector[10*j+0] != Big_Num) && (Input_vector[10*j+1] != Big_Num) && (Input_vector[10*j+0] + Input_vector[10*j+2] > Input_vector[10*i+0]) && (Input_vector[10*j+0] < Input_vector[10*i+0] + Input_vector[10*i+2])) {
					Adj_Vector[j] = 1.0;
				}//if
			}//j
			//An_Added_11(i, :) = [Boxes(i, 1:10), bi2de(Adj_Vector)];
			out[11 * i + 0] = Input_vector[10 * i + 0];
			out[11 * i + 1] = Input_vector[10 * i + 1];
			out[11 * i + 2] = Input_vector[10 * i + 2];
			out[11 * i + 3] = Input_vector[10 * i + 3];
			out[11 * i + 4] = Input_vector[10 * i + 4];
			out[11 * i + 5] = Input_vector[10 * i + 5];
			out[11 * i + 6] = Input_vector[10 * i + 6];
			out[11 * i + 7] = Input_vector[10 * i + 7];
			out[11 * i + 8] = Input_vector[10 * i + 8];
			out[11 * i + 9] = Input_vector[10 * i + 9];
			out[11 * i + 10] = Mybi2dec(Adj_Vector);
		}//if
		else {
			//An_Added_11(i, :) = [Boxes(i, 1:10), Big_Num];
			out[11 * i + 0] = Input_vector[10 * i + 0];
			out[11 * i + 1] = Input_vector[10 * i + 1];
			out[11 * i + 2] = Input_vector[10 * i + 2];
			out[11 * i + 3] = Input_vector[10 * i + 3];
			out[11 * i + 4] = Input_vector[10 * i + 4];
			out[11 * i + 5] = Input_vector[10 * i + 5];
			out[11 * i + 6] = Input_vector[10 * i + 6];
			out[11 * i + 7] = Input_vector[10 * i + 7];
			out[11 * i + 8] = Input_vector[10 * i + 8];
			out[11 * i + 9] = Input_vector[10 * i + 9];
			out[11 * i + 10] = Big_Num;
		}//else
	}//i

	for (int j = 0; j < 220; j++) {
		Output_vector[j] = out[j];
	}

}//function


//Input columns(11): [Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location, Box_Config, Adj_Vector]
//output columns(12):[Box_S, Box_N, Box_Length, Box_Width, Salience, EgoCar_Lane, Box_Lane, Absolute_Box_Lane, Relative_Location, Box_Config, Adj_Vector, Whole_Config]
void Add_Whole_Config(double Input_vector[], double Output_vector[]) {

	double Big_Num = 1000.0;
	double Config_Number = 0.0;
	double Config_Vec[8] = { };
	double out[240] = { };

	for (int j = 0; j < 20; j++){
		if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 1)) {
			//Config_Matrix(2, 1) = 1;
			Config_Vec[0] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 2)) {
			//Config_Matrix(1, 1) = 1;
			Config_Vec[1] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 3) && (Input_vector[11*j+4] == 1) && (Config_Vec[2] != 2)) {
			//Config_Matrix(2, 2) = 0;
			Config_Vec[2] = 1.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 3) && (Input_vector[11*j+4] == -1)) {
			//Config_Matrix(2, 2) = 1;
			Config_Vec[2] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 4) && (Input_vector[11*j+4] == 1) && (Config_Vec[3] != 2)) {
			//Config_Matrix(1, 2) = 0;
			Config_Vec[3] = 1.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 4) && (Input_vector[11*j+4] == -1)) {
			//Config_Matrix(1, 2) = 1;
			Config_Vec[3] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 5) && (Input_vector[11*j+4] == 1)) {
			//Config_Matrix(2, 3) = 0;
			Config_Vec[4] = 1.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 5) && (Input_vector[11*j+4] == -1)) {
			//Config_Matrix(2, 3) = 1;
			Config_Vec[4] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 6) && (Input_vector[11*j+4] == 1)) {
			//Config_Matrix(1, 3) = 0;
			Config_Vec[5] = 1.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 6) && (Input_vector[11*j+4] == -1)) {
			//Config_Matrix(1, 3) = 1;
			Config_Vec[5] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 7) && (Input_vector[11*j+4] == 1)) {
			//Config_Matrix(2, 4) = 0;
			Config_Vec[6] = 1.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 7) && (Input_vector[11*j+4] == -1)) {
			//Config_Matrix(2, 4) = 1;
			Config_Vec[6] = 2.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 8) && (Input_vector[11*j+4] == 1)) {
			//Config_Matrix(1, 4) = 0;
			Config_Vec[7] = 1.0;
		}
		else if ((Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) && (Input_vector[11*j+9] == 8) && (Input_vector[11*j+4] == -1)) {
			//Config_Matrix(1, 4) = 1;
			Config_Vec[7] = 2.0;
		}
		else {
			//%Dummy = 0; %just to do nothing.
		}

	}//j

	Config_Number = Config_Vec[0] * 1.0 + Config_Vec[1] * 3.0 + Config_Vec[2] * 9.0 + Config_Vec[3] * 27.0 + Config_Vec[4] * 81.0 + Config_Vec[5] * 243.0 + Config_Vec[6] * 729.0 + Config_Vec[7] * 2187.0;

	for (int j = 0; j < 20; j++) {
		if ( (Input_vector[11*j+0] != Big_Num) && (Input_vector[11*j+1] != Big_Num) ) {
			//An_Added_12(i, :) = [Boxes(i, 1:11), Config_Number];
			out[12 * j + 0] =  Input_vector[11 * j + 0];
			out[12 * j + 1] =  Input_vector[11 * j + 1];
			out[12 * j + 2] =  Input_vector[11 * j + 2];
			out[12 * j + 3] =  Input_vector[11 * j + 3];
			out[12 * j + 4] =  Input_vector[11 * j + 4];
			out[12 * j + 5] =  Input_vector[11 * j + 5];
			out[12 * j + 6] =  Input_vector[11 * j + 6];
			out[12 * j + 7] =  Input_vector[11 * j + 7];
			out[12 * j + 8] =  Input_vector[11 * j + 8];
			out[12 * j + 9] =  Input_vector[11 * j + 9];
			out[12 * j + 10] = Input_vector[11 * j + 10];
			out[12 * j + 11] = Config_Number;
		}//if
		else {
			//An_Added_12(i, :) = [Boxes(i, 1:11), Big_Num];
			out[12 * j + 0] =  Input_vector[11 * j + 0];
			out[12 * j + 1] =  Input_vector[11 * j + 1];
			out[12 * j + 2] =  Input_vector[11 * j + 2];
			out[12 * j + 3] =  Input_vector[11 * j + 3];
			out[12 * j + 4] =  Input_vector[11 * j + 4];
			out[12 * j + 5] =  Input_vector[11 * j + 5];
			out[12 * j + 6] =  Input_vector[11 * j + 6];
			out[12 * j + 7] =  Input_vector[11 * j + 7];
			out[12 * j + 8] =  Input_vector[11 * j + 8];
			out[12 * j + 9] =  Input_vector[11 * j + 9];
			out[12 * j + 10] = Input_vector[11 * j + 10];
			out[12 * j + 11] = Big_Num;
		}//else
	}//j

	for (int j = 0; j < 240; j++) {
		Output_vector[j] = out[j];
	}

}//function

void Add_Annotations(double Input_vector[], double Output_vector[], int& N_R) {

	double Big_Num = 1000.0;
	double out[260] = { };
	int N_outputs = 0;

	for (int j = 0; j < 20; j++){
		out[13 * j + 0] = Input_vector[12 * j + 0];
		out[13 * j + 1] = Input_vector[12 * j + 1];
		out[13 * j + 2] = Input_vector[12 * j + 2];
		out[13 * j + 3] = Input_vector[12 * j + 3];
		out[13 * j + 4] = Input_vector[12 * j + 4];
		out[13 * j + 5] = Input_vector[12 * j + 5];
		out[13 * j + 6] = Input_vector[12 * j + 6];
		out[13 * j + 7] = Input_vector[12 * j + 7];
		out[13 * j + 8] = Input_vector[12 * j + 8];
		out[13 * j + 9] = Input_vector[12 * j + 9];
		out[13 * j + 10] = Input_vector[12 * j + 10];
		out[13 * j + 11] = Input_vector[12 * j + 11];
		////////////////////  0 Car /////////////////////////////////////////////
		if ( (Input_vector[12*j+0] != Big_Num) && (Input_vector[12*j+1] != Big_Num) && (Input_vector[12*j+11] == 36.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 36.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
			////////////////////  1 Cars /////////////////////////////////////////////
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 42.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 42.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 38.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 38.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2709.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2709.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 927.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 927.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 198.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 198.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 306.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 306.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 126.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 126.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 45.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 45.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
			////////////////////  2 Cars /////////////////////////////////////////////
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 44.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 44.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4680.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4680.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1197.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1197.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 308.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 308.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2711.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2711.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2715.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2715.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3600.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3600.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1584.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1584.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2799.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2799.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 132.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 132.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 929.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 929.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 933.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 933.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 468.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 468.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 396.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 396.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2871.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2871.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
			////// 3Cars ///////////////////////////////////////////////////
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2717.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2717.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 935.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 935.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4682.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4682.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 5571.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 5571.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1199.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1199.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3602.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3602.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3606.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3606.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1950.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 1950.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4257.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4257.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2805.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 2805.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 5577.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 5577.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
			////// 4Cars ///////////////////////////////////////////////////
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3608.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 3608.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 5573.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 5573.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 51.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 102060.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4263.0) && (Input_vector[12 * j + 5] == 4.0)) {
			if (Input_vector[12 * j + 9] == 3.0) { out[13 * j + 12] = 102060.0; }
			else if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 31.0; }
		}
		else if ((Input_vector[12 * j + 0] != Big_Num) && (Input_vector[12 * j + 1] != Big_Num) && (Input_vector[12 * j + 11] == 4263.0) && (Input_vector[12 * j + 5] == 3.0)) {
			if (Input_vector[12 * j + 9] == 4.0) { out[13 * j + 12] = 10204160.0; }
		}
		else {
			out[13 * j + 12] = 0.0;
		}

	}//j

	for (int j = 0; j < 20; j++) {
		if ( ( (out[13 * j + 0] != Big_Num) && (out[13 * j + 1] != Big_Num) && (out[13 * j + 2] != Big_Num)) && !((out[13 * j + 0] == 0.0) && (out[13 * j + 1] == 0.0) && (out[13 * j + 2] == 0.0)) ) {
			N_outputs = N_outputs + 1;
		}//if
	}//j

	for (int j = 0; j < 260; j++) {
		Output_vector[j] = out[j];
	}//j

	N_R = N_outputs;

//	cout << "\t";
//	cout << N_outputs;
//	cout << endl;
}//function

void Add_Num_of_Rows(int N_R, double Input_vector[], double Output_vector[]) {
	double Big_Num = 1000.0;
	double out[280] = { };

	for (int j = 0; j < 20; j++){
		out[14 * j + 0] =  Input_vector[13 * j + 0];
		out[14 * j + 1] =  Input_vector[13 * j + 1];
		out[14 * j + 2] =  Input_vector[13 * j + 2];
		out[14 * j + 3] =  Input_vector[13 * j + 3];
		out[14 * j + 4] =  Input_vector[13 * j + 4];
		out[14 * j + 5] =  Input_vector[13 * j + 5];
		out[14 * j + 6] =  Input_vector[13 * j + 6];
		out[14 * j + 7] =  Input_vector[13 * j + 7];
		out[14 * j + 8] =  Input_vector[13 * j + 8];
		out[14 * j + 9] =  Input_vector[13 * j + 9];
		out[14 * j + 10] = Input_vector[13 * j + 10];
		out[14 * j + 11] = Input_vector[13 * j + 11];
		out[14 * j + 12] = Input_vector[13 * j + 12];
		out[14 * j + 13] = N_R;
//			out[24 * j + 14] = 0.0;
//			out[24 * j + 15] = 0.0;
//			out[24 * j + 16] = 0.0;
//			out[24 * j + 17] = 0.0;
//			out[24 * j + 18] = 0.0;
//			out[24 * j + 19] = 0.0;
//			out[24 * j + 20] = 0.0;
//			out[24 * j + 21] = 0.0;
//			out[24 * j + 22] = 0.0;
//			out[24 * j + 23] = 0.0;
	}//j

	for (int j = 0; j < 280; j++) {
		Output_vector[j] = out[j];
	}

}//function

void Mysort(double Input_vector[], double Output_vector[]) {
	double temp[7] = { 0,0,0,0,0,0,0 };//for 5x3 matrix
	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 7; j++) {
			Output_vector[i * 7 + j] = Input_vector[i * 7 + j];
		}
	}
	//for (int i = 0; i < 5 - 1; i++) {
	for (int i = 0; i < 20 - 1; i++) {
		//for (int j = 0; j < 5-i; j++) {
		for (int j = 0; j < 20 - i - 1; j++) {
			//if (Input_vector[3*j] < Input_vector[3*(j+1)]) {
			if (Output_vector[7 * j] < Output_vector[7 * (j + 1)]) {
				//temp[0] = Input_vector[3 * j + 0];
				temp[0] = Output_vector[7 * j + 0];
				temp[1] = Output_vector[7 * j + 1];
				temp[2] = Output_vector[7 * j + 2];
				temp[3] = Output_vector[7 * j + 3];
				temp[4] = Output_vector[7 * j + 4];
				temp[5] = Output_vector[7 * j + 5];
				temp[6] = Output_vector[7 * j + 6];
				//Input_vector[3 * j + 0] = Input_vector[3 * (j + 1) + 0];
				Output_vector[7 * j + 0] = Output_vector[7 * (j + 1) + 0];
				Output_vector[7 * j + 1] = Output_vector[7 * (j + 1) + 1];
				Output_vector[7 * j + 2] = Output_vector[7 * (j + 1) + 2];
				Output_vector[7 * j + 3] = Output_vector[7 * (j + 1) + 3];
				Output_vector[7 * j + 4] = Output_vector[7 * (j + 1) + 4];
				Output_vector[7 * j + 5] = Output_vector[7 * (j + 1) + 5];
				Output_vector[7 * j + 6] = Output_vector[7 * (j + 1) + 6];
				//Input_vector[3 * (j + 1) + 0] = temp[0];
				Output_vector[7 * (j + 1) + 0] = temp[0];
				Output_vector[7 * (j + 1) + 1] = temp[1];
				Output_vector[7 * (j + 1) + 2] = temp[2];
				Output_vector[7 * (j + 1) + 3] = temp[3];
				Output_vector[7 * (j + 1) + 4] = temp[4];
				Output_vector[7 * (j + 1) + 5] = temp[5];
				Output_vector[7 * (j + 1) + 6] = temp[6];
			}//if
		}//j
	}//i
	//for (int i = 0; i < 2; i++) {
	for (int i = 0; i < 10; i++) {
		//temp[0] = Input_vector[3 * i + 0];
		temp[0] = Output_vector[7 * i + 0];
		temp[1] = Output_vector[7 * i + 1];
		temp[2] = Output_vector[7 * i + 2];
		temp[3] = Output_vector[7 * i + 3];
		temp[4] = Output_vector[7 * i + 4];
		temp[5] = Output_vector[7 * i + 5];
		temp[6] = Output_vector[7 * i + 6];
		//Input_vector[3 * i + 0] = Input_vector[3 * (4 - i) + 0];
		Output_vector[7 * i + 0] = Output_vector[7 * (19 - i) + 0];
		Output_vector[7 * i + 1] = Output_vector[7 * (19 - i) + 1];
		Output_vector[7 * i + 2] = Output_vector[7 * (19 - i) + 2];
		Output_vector[7 * i + 3] = Output_vector[7 * (19 - i) + 3];
		Output_vector[7 * i + 4] = Output_vector[7 * (19 - i) + 4];
		Output_vector[7 * i + 5] = Output_vector[7 * (19 - i) + 5];
		Output_vector[7 * i + 6] = Output_vector[7 * (19 - i) + 6];
		//Input_vector[3 * (i + 4) + 0] = temp[0];
		Output_vector[7 * (19 - i) + 0] = temp[0];
		Output_vector[7 * (19 - i) + 1] = temp[1];
		Output_vector[7 * (19 - i) + 2] = temp[2];
		Output_vector[7 * (19 - i) + 3] = temp[3];
		Output_vector[7 * (19 - i) + 4] = temp[4];
		Output_vector[7 * (19 - i) + 5] = temp[5];
		Output_vector[7 * (19 - i) + 6] = temp[6];
	}//i
}//function

double Mymax(double num1, double num2) {
	double result;
	if (num1 > num2)
		result = num1;
	else
		result = num2;

	return result;
}

void Add_Gaps_One_Lane(double num2, double Input_vector[], double Output_vector[]) {
	double Car_Length = 4.8;
	double Car_Width = 2.0;
	double Big_Num = 1000.0;
	double Max_Length_of_Free_Gap = 150.0;
	double Lane_Width = 3.5;

	int Gap_Added_Tag = 0;
	int Last_Obj_Index = 0;
	int Another_Car_Index = 0;
	int Gap1_S = 0;

	double Box_S = Big_Num;
	double Box_N = Big_Num;
	double Box_Width = Big_Num;
	double Box_Length = Big_Num;
	double Salience = Big_Num;
	double EgoCar_Lane = Big_Num;
	double Box_Lane = Big_Num;
	double out[140] = { };

	for (int j = 1; j < 20; j++){
		//see if the Box is behind the Ego Car.
		if ((Input_vector[7 * j + 0] != Big_Num) && (Input_vector[7 * j + 1] != Big_Num) && (Input_vector[7 * j + 4] == -1) && (Input_vector[7 * j + 6] == num2) && (Input_vector[7 * j + 0] < Car_Length)) {
			Gap1_S = Mymax(0.0, Input_vector[7*j+0] + Input_vector[7*j+2]);
		}//if
	}//j

	for (int j = 0; j < 20; j++){
		if ((Input_vector[7*j+0] != Big_Num) && (Input_vector[7*j+1] != Big_Num) && (Input_vector[7*j+4] == -1) && (Input_vector[7*j+6] == num2) && (Input_vector[7*j+0] > Car_Length)) {
			Box_S = Mymax(0.0, Gap1_S);
			Box_N = num2 * Lane_Width;
			Box_Width = Input_vector[7*j+3];
			Box_Length = Input_vector[7*j+0] - Box_S;
			Salience = 1.0;
			EgoCar_Lane = Input_vector[7*j+5];
			Box_Lane = Input_vector[7*j+6];
			Gap_Added_Tag = Gap_Added_Tag + 1;
			Last_Obj_Index = j;
			//Another_Car_Index calculation
			for (int k = j + 1; k < 20; k++){
				if ((Input_vector[7*k+0] != Big_Num) && (Input_vector[7*k+1] != Big_Num) && (Input_vector[7*k+4] == -1) && (Input_vector[7*k+6] == num2)) {
					Another_Car_Index = k;
					break;
				}//if
			}//k
			break;
		}//if
		Gap_Added_Tag = Gap_Added_Tag;
	}//j
	//-------------

	if (Gap_Added_Tag != 0.0){
		//Inserting the first Gap in the matrix. ------------------------------------------------
		for (int j = 0; j < 20; j++) {
			if ((Input_vector[7*j+0] == Big_Num) && (Input_vector[7*j+1] == Big_Num)) {
				Input_vector[7 * j + 0] = Box_S;
				Input_vector[7 * j + 1] = Box_N;
				Input_vector[7 * j + 2] = Box_Length;
				Input_vector[7 * j + 3] = Box_Width;
				Input_vector[7 * j + 4] = Salience;
				Input_vector[7 * j + 5] = EgoCar_Lane;
				Input_vector[7 * j + 6] = Box_Lane;
				break;
			}//if
		}//j  --------------------------------------------------------------
		//Now the first Gap is inserted.Is there another Car ?
		if (Another_Car_Index == 0.0) {
			Box_S = Input_vector[7*Last_Obj_Index+0] + Input_vector[7*Last_Obj_Index+2];
			Box_N = num2 * Lane_Width;
			Box_Width = Input_vector[7* Last_Obj_Index +3];
			Box_Length = Max_Length_of_Free_Gap;
			Salience = 1.0;
			EgoCar_Lane = Input_vector[7* Last_Obj_Index +5];
			Box_Lane = Input_vector[7* Last_Obj_Index+6];
			Gap_Added_Tag = Gap_Added_Tag + 1;
		}//if
		else {
			Box_S = Mymax(Input_vector[7*Last_Obj_Index+0] + Input_vector[7*Last_Obj_Index+2], 0);
			Box_N = num2 * Lane_Width;
			Box_Width = Input_vector[7* Another_Car_Index+3];
			Box_Length = Input_vector[7*Another_Car_Index+0] - Box_S;
			Salience = 1.0;
			EgoCar_Lane = Input_vector[7* Another_Car_Index+5];
			Box_Lane = Input_vector[7* Another_Car_Index+6];
			Gap_Added_Tag = Gap_Added_Tag + 1;
		}//else
		//Now the second Gap is inserted.
		//Adding the second Gap. ------------------------------------------ -
		for (int j = 0; j < 20; j++) {
			if ((Input_vector[7*j+0] == Big_Num) && (Input_vector[7*j+1] == Big_Num)) {
				Input_vector[7 * j + 0] = Box_S;
				Input_vector[7 * j + 1] = Box_N;
				Input_vector[7 * j + 2] = Box_Length;
				Input_vector[7 * j + 3] = Box_Width;
				Input_vector[7 * j + 4] = Salience;
				Input_vector[7 * j + 5] = EgoCar_Lane;
				Input_vector[7 * j + 6] = Box_Lane;
				break;
			}//if
		}//j ----------------------------------------------------------
		Mysort(Input_vector, out);
	}//if (Gap_Added_Tag != 0.0)

	else {//% (Gap_Added_Tag == 0)
		//Adding an endless Gap. -------------------------------------------- -
		Box_S = Mymax(0.0, Gap1_S);
		Box_N = num2 * Lane_Width;
		Box_Width = Car_Width;
		Box_Length = Max_Length_of_Free_Gap;
		Salience = 1.0;
		EgoCar_Lane = Input_vector[7*0+5];
		Box_Lane = num2;
		//Inserting the Gap in the matrix. ------------------------------------------ -
		for (int j = 0; j < 20; j++) {
			if ((Input_vector[7*j+0] == Big_Num) && (Input_vector[7*j+1] == Big_Num)) {
				Input_vector[7 * j + 0] = Box_S;
				Input_vector[7 * j + 1] = Box_N;
				Input_vector[7 * j + 2] = Box_Length;
				Input_vector[7 * j + 3] = Box_Width;
				Input_vector[7 * j + 4] = Salience;
				Input_vector[7 * j + 5] = EgoCar_Lane;
				Input_vector[7 * j + 6] = Box_Lane;
				break;
			}//if
		}//j  -------------------------------------------------------------
		Mysort(Input_vector,out);
	}//(Gap_Added_Tag != 0)


	for (int j = 0; j < 140; j++) {
		Output_vector[j] = out[j];
	}

}//function

double Mybi2dec(double Input_vector[]) {
	double result;
	double out = 0.0;

	out = 1 * Input_vector[0] + 2 * Input_vector[1] + 4 * Input_vector[2] + 8 * Input_vector[3] + 16 * Input_vector[4] + 32 * Input_vector[5] + 64 * Input_vector[6] + 128 * Input_vector[7] + 256 * Input_vector[8] + 512 * Input_vector[9] + 1024 * Input_vector[10] + 2048 * Input_vector[11];
	result = out;
	return result;
}

#else
#endif

void LogicalReasoningModuleInit ()
{
;
}
