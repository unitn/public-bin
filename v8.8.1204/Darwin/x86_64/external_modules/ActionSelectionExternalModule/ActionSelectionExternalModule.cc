#include "ActionSelectionExternalModule.h"
#include <math.h>

double wLl = 1.0;		/* weight for change lane left */
double wLc = 1.0;		/* weight for stay in current lane */
double wLr = 1.0;		/* weight for change right  */
double wA = 1.0;		/* weight for advance (i.e. accelerate) */
double wD = 1.0;		/* weight for deccelerate  */
double wThrsh = 0.001;	/* weight for change MSPRT thrshold */
int forgFactor = 5; /* forgetting factor */

void ActionSelectionModuleInit ()
{
    int i;
    int j;
    int k;
    MotorCortexHistory.SetShape(Max(20,0), Max(41,0), Max(41,0) );
    k = 1;
    while (k <= 20)
    {
        j = 1;
        while (j <= 41)
        {
            i = 1;
            while (i <= 41)
            {
                MotorCortexHistory(k, j, i) = 0.;
                i = i+1;
            }
            j = j+1;
        }
        k = k+1;
    }
    MotorCortexHistorySize = 0;;
    MaxEvidence = 0.;
    ActionSelectionType = 0;

}

doubleNN BiasAndActionSelection (const doubleNN &MotorCortex, const doubleNNN &BiasMatrices, const doubleN &SemanticCodes, const doubleN &GuessWeights, const int &ActionSelectionAlg)
{
    doubleN Weights(0);
    doubleNN GainMatrix(0, 0);
    doubleNN InhibitionMatrix(0, 0);
    doubleNN MotorCortexOut(0, 0);
    int j;
    int i;

	/*The initial weight values*/
	/*Here is where Reinforcement Learning can be used to modify Weights - using SemanticCodes as a descriptor of the current situation*/
    Weights = GuessWeights;

	/*Set the dimensions of the GainMatrix*/
	/*Weights can be either positive or negative*/
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2),0), Max(BiasMatrices.dimension(3),0));

    i = 1;
    while (i <= BiasMatrices.dimension(2))
    {
        j = 1;
        while (j <= BiasMatrices.dimension(3))
        {
            GainMatrix(i, j) = 1.+BiasMatrices(1, i, j)*Weights(1)+LightMax(ElemProduct(BiasMatrices(R(2,-1), i, j),Weights(R(2,-1))));
            j = j+1;
        }
        i = i+1;
    }

	/*Set the dimensions of the InhibitionMatrix*/
    InhibitionMatrix.SetShape(Max(BiasMatrices.dimension(2),0), Max(BiasMatrices.dimension(3),0));

    i = 1;
    while (i <= BiasMatrices.dimension(2))
    {
        j = 1;
        while (j <= BiasMatrices.dimension(3))
        {
            InhibitionMatrix(i, j) = 1.-LightMax(ElemProduct(BiasMatrices(R(2,-1), i, j), -Weights(R(2,-1))));
            j = j+1;
        }
        i = i+1;
    }

	/*Set the dimensions of the GainMatrix*/
    GainMatrix.SetShape(Max(BiasMatrices.dimension(2),0), Max(BiasMatrices.dimension(3),0) );

    i = 1;
    while (i <= BiasMatrices.dimension(2))
    {
        j = 1;
        while (j <= BiasMatrices.dimension(3))
        {
            GainMatrix(1+i+-1, 1+j+-1) = GainMatrix(i, j)*Max(0.,InhibitionMatrix(i, j));
            j = j+1;
        }
        i = i+1;
    }

    if (ActionSelectionAlg == 1)
    {
        MotorCortexOut = MSPRT (MotorCortex, GainMatrix);

    }
    else
    {
        MotorCortexOut = WTA (MotorCortex, GainMatrix);
    }
    return - MotorCortexOut;
}

int getMotorCortexHistorySize ()
{
    return MotorCortexHistorySize;
}

int getActionSelectionType ()
{
  return ActionSelectionType;;
}

doubleNNN getMotorCortexHistory ()
{
    return MotorCortexHistory;
}

double getMaxEvidence ()
{
    return MaxEvidence;
}


doubleNN MSPRT ( const doubleNN &MotorCortex, const
    doubleNN &GainMatrix)
{
    int i;
    int j;
    doubleNN MotorCortexHistoryTotal(0, 0);
    doubleNN MotorCortexOut(0, 0);
    double threshold;
    int forgettingFactor;
    int maxMotorCortexHistorySize;
    int shiftSize;


    threshold = wThrsh;
    ActionSelectionType = 0;
    forgettingFactor = forgFactor;
    maxMotorCortexHistorySize = MotorCortexHistory
        .dimension(1);
    MotorCortexHistorySize = Min(
        MotorCortexHistorySize+1,
        maxMotorCortexHistorySize);
    MotorCortexHistory.Set(
        MotorCortexHistorySize,All,All,MotorCortex);
    MotorCortexHistoryTotal = light_total(
        MotorCortexHistory(R(1,
        MotorCortexHistorySize),All,All));
    MotorCortexOut = ElemProduct (GainMatrix, MotorCortexHistoryTotal)+-log(
        light_total(light_flatten(exp(ElemProduct (GainMatrix,
        MotorCortexHistoryTotal)))));
        MaxEvidence = LightMax(exp(light_flatten(
            MotorCortexOut)));
    if (MaxEvidence >= threshold)
    {
        shiftSize = Min(forgettingFactor,
            MotorCortexHistorySize);
        MotorCortexHistory.Set(R(1,shiftSize),All,All,
            MotorCortexHistory(R(
            MotorCortexHistorySize+-shiftSize+1,
            MotorCortexHistorySize),All,All));
        MotorCortexHistorySize = shiftSize;
        ActionSelectionType = 1;
    }
    else
    {
        if (MotorCortexHistorySize ==
            maxMotorCortexHistorySize)
        {
            shiftSize = Min(forgettingFactor,
                MotorCortexHistorySize);
            MotorCortexHistory.Set(R(1,shiftSize),All,All
                ,MotorCortexHistory(R(
                MotorCortexHistorySize+-shiftSize+1,
                MotorCortexHistorySize),All,All));
            MotorCortexHistorySize = shiftSize;
            ActionSelectionType = 2;
        }
    }
    return MotorCortexOut;
}

doubleNN WTA ( const doubleNN &MotorCortex, const
    doubleNN &GainMatrix)
{
    doubleNN MotorCortexOut(0, 0);

    ActionSelectionType = 3;
    return MotorCortexOut = ElemProduct (GainMatrix, MotorCortex);
}

double MaxEvidence;
int MotorCortexHistorySize;
int ActionSelectionType;
doubleNNN MotorCortexHistory(20, 41, 41);
